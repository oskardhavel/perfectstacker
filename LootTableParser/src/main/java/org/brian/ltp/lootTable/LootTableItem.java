package org.brian.ltp.lootTable;

import org.brian.ltp.lootTable.function.LootTableFunction;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public class LootTableItem {

    private Material material;
    private List<LootTableFunction> functionList = new ArrayList<>();

    public LootTableItem(Material material){

        this.material = material;

    }

    public Material material(){
        return material;
    }

    public List<LootTableFunction> functionList(){
        return functionList;
    }

    public LootTableItem addFunction(LootTableFunction function){
        this.functionList.add(function);
        return this;
    }

}
