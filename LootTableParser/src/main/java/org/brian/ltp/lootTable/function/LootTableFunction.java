package org.brian.ltp.lootTable.function;

import java.util.concurrent.ThreadLocalRandom;

public class LootTableFunction {

    public enum FunctionType {

        SET_COUNT,
        LOOTING_ENCHANT

    }

    private FunctionType type;
    private int max;
    private int min;

    public LootTableFunction(FunctionType type, int min, int max){

        this.type = type;
        this.min = min;
        this.max = max;

    }

    public FunctionType type(){
        return type;
    }

    public int max(){
        return max;
    }

    public int min(){
        return max;
    }

    public int generate(){
        return min == max ? min : ThreadLocalRandom.current().nextInt(min, max);
    }

}
