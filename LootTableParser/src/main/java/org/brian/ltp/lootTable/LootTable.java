package org.brian.ltp.lootTable;

import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

public class LootTable {

    private EntityType owner;
    private List<LootTableItem> drops = new ArrayList<>();

    public LootTable(EntityType owner){
        this.owner = owner;
    }

    public LootTable addLootTableItem(LootTableItem tableItem){
        this.drops.add(tableItem);
        return this;
    }

    public EntityType owner(){
        return owner;
    }

    public List<LootTableItem> lootTableItemList(){
        return drops;
    }

}
