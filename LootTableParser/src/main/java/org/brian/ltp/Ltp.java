package org.brian.ltp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import org.brian.ltp.lootTable.LootTable;
import org.brian.ltp.lootTable.LootTableItem;
import org.brian.ltp.lootTable.function.LootTableFunction;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ltp {

    ClassLoader classLoader = Ltp.class.getClassLoader();
    File nmsEntitiesFolder = new File(classLoader.getResource("nmsEntities").getFile());

    public static void main(String[] args){

        ClassLoader classLoader = Ltp.class.getClassLoader();
        File nmsEntitiesFolder = new File(classLoader.getResource("nmsEntities").getFile());

        Gson GSON = new GsonBuilder().
                setPrettyPrinting().
                create();
        try {

            if (nmsEntitiesFolder.exists() || nmsEntitiesFolder.isDirectory()) {

                List<LootTable> lootTableList = new ArrayList<>();

                for (File file : nmsEntitiesFolder.listFiles((dir, name) -> name.contains(".json"))) {

                    String entityType = file.getName().replace(".json", "");
                    if(entityType.equalsIgnoreCase("SNOW_GOLEM")) entityType = EntityType.SNOWMAN.name();
                    else if(entityType.equalsIgnoreCase("ZOMBIE_PIGMAN")) entityType = EntityType.PIG_ZOMBIE.name();
                    else if(entityType.equalsIgnoreCase("MOOSHROOM")) entityType = "MUSHROOM_COW";

                    EntityType type = EntityType.valueOf(entityType.toUpperCase());
                    System.out.println("Initing " + type);

                    LootTable lootTable = new LootTable(type);
                    lootTableList.add(lootTable);

                    Map<String, Object> fileData = new HashMap<>();
                    fileData = GSON.fromJson(new FileReader(file), fileData.getClass());

                    if(fileData.containsKey("pools")){

                        List<Object> pools = toList(fileData.get("pools"));
                        for(Object pool : pools){

                            Map<String, Object> poolData = toMap(pool);
                            List<Object> entriesData = toList(poolData.get("entries"));

                            for(Object entry : entriesData){

                                Map<String, Object> entryData = toMap(entry);
                                if(!entryData.containsKey("name") || !entryData.get("type").toString().equalsIgnoreCase("minecraft:item")) continue;

                                String stringMaterial = entryData.get("name").toString().split(":")[1];
                                Material material = Material.valueOf(stringMaterial.toUpperCase());

                                LootTableItem lootTableItem = new LootTableItem(material);

                                System.out.println("  Initing Drop " + material);

                                if (entryData.containsKey("functions")) {

                                    List<Object> functions = toList(entryData.get("functions"));
                                    for(Object function : functions){

                                        Map<String, Object> functionData = toMap(function);
                                        if(!functionData.containsKey("function") || !functionData.containsKey("count")) continue;

                                        String functionName = functionData.get("function").toString().split(":")[1];
                                        LootTableFunction.FunctionType functionType = LootTableFunction.FunctionType.valueOf(functionName.toUpperCase());
                                        System.out.println("    Initing function "  + functionType);

                                        int min, max;

                                        if(functionData.get("count") instanceof Double){

                                            max = (int) Math.round(Double.parseDouble(functionData.get("count").toString()));
                                            min = 1;

                                        } else {

                                            Map<String, Object> countData = toMap(functionData.get("count"));

                                            min = (int) Math.round(Double.parseDouble((countData.get("min").toString())));
                                            max = (int) Math.round(Double.parseDouble((countData.get("max").toString())));

                                            if (min < 0) min = 1;
                                            if (max < 0) max = 1;

                                        }

                                        lootTableItem.addFunction(new LootTableFunction(functionType, min, max));

                                    }

                                    lootTable.addLootTableItem(lootTableItem);

                                }

                            }

                        }

                    }

                }

                //Load Sheep
                //Later


                //Save
                File ltFolder = new File(nmsEntitiesFolder.getPath().replace("nmsEntities", "lt"));
                if(!ltFolder.exists()) ltFolder.mkdirs();

                for(LootTable table : lootTableList) {

                    File tableFile = new File(ltFolder, table.owner().name().toLowerCase() + ".json");
                    if(!tableFile.exists()) tableFile.createNewFile();

                    FileWriter writer = new FileWriter(tableFile);

                    GSON.toJson(table, writer);

                    writer.flush();
                    writer.close();

                }

                //Load
                GSON = new Gson();
                for(File file : ltFolder.listFiles()) {

                    LootTable table = GSON.fromJson(new FileReader(file), LootTable.class);
                    table.lootTableItemList().forEach(i -> System.out.println(i.material()));

                }

            }

        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    static List<Object> toList(Object object){
        return (List<Object>) object;
    }

    static Map<String, Object> toMap(Object object){
        return (Map<String, Object>)object;
    }

}
