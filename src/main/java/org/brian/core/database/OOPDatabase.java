package org.brian.core.database;

import java.sql.*;
import java.util.HashMap;

public abstract class OOPDatabase {

    private SDatabaseType type;
    private String url;

    public OOPDatabase(String url, SDatabaseType type){

        this.type = type;
        this.url = url;

    }

    public abstract Connection getConnection();
    public abstract Connection getConnection(boolean openNew);

    public enum SDatabaseType {

        SQLITE,
        MYSQL

    }

    public <T extends OOPDatabase> T to(Class<T> to){

        return to.cast(this);

    }

    public void cleanTable(String name){

        run("DELETE FROM " + name);

    }



    public void createTable(String name, String... rows){

        StringBuilder sqlBuilder = new StringBuilder("CREATE TABLE IF NOT EXISTS " + name + " (");

        int currentRow = 1;
        for(String row : rows){

            if(currentRow < rows.length) sqlBuilder.append(row + ", ");
            else sqlBuilder.append(row + ")");
            currentRow++;

        }
        run(sqlBuilder.toString());

    }

    public String getUrl() {
        return url;
    }

    public SDatabaseType getType() {
        return type;
    }

    public void run(String sql){

        try {

            Connection conn = getConnection(true);
            if (conn == null) return;
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();

        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public HashMap<Integer, HashMap<String, Object>> getAllRows(String tableName) {

        HashMap<Integer, HashMap<String, Object>> ret = new HashMap<>();
        Connection conn = getConnection();

        try {
            try (PreparedStatement selectStmt = conn.prepareStatement(
                    "SELECT * from " + tableName, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                 ResultSet rs = selectStmt.executeQuery()) {
                if (!rs.isBeforeFirst()) {
                } else {
                    int id = 0;
                    while (rs.next()) {
                        for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
                            if (rs.getMetaData().getColumnName(i).equalsIgnoreCase("id")) {

                                id = (int) rs.getObject(i);
                                HashMap<String, Object> data = new HashMap<>();
                                ret.put(id, data);


                            } else {

                                ret.get(id).put(rs.getMetaData().getColumnName(i), rs.getObject(i));

                            }
                        }
                    }
                    rs.close();
                    selectStmt.close();
                    conn.close();
                }
            }
            conn.close();
        } catch (SQLException e) {
            return ret;
        }

        return ret;
    }

}
