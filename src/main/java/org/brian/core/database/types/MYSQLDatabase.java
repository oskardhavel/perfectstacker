package org.brian.core.database.types;


import org.brian.core.database.OOPDatabase;

import java.sql.Connection;
import java.sql.DriverManager;

public class MYSQLDatabase extends OOPDatabase {

    private String password;
    private String user;
    private Connection lastConnection;

    public MYSQLDatabase(String host, int port, String database, String user, String password) {
        super("jdbc:mysql://" + host + ":" + port + "/" + database, SDatabaseType.MYSQL);
        this.password = password;
        this.user = user;
    }

    @Override
    public Connection getConnection() {

        try{

            if(lastConnection == null || lastConnection.isClosed()) {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                lastConnection = DriverManager.getConnection(getUrl(), user, password);
                return lastConnection;
            } else return lastConnection;

        } catch (Exception ex){
            ex.printStackTrace();
        }

        return null;

    }

    @Override
    public Connection getConnection(boolean openNew) {

        try{

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            lastConnection = DriverManager.getConnection(getUrl(), user, password);
            return lastConnection;

        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;

    }
}
