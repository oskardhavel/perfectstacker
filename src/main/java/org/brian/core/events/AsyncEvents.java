package org.brian.core.events;

import org.brian.core.Core;
import org.brian.core.energiser.ETask;
import org.brian.core.energiser.Energiser;
import org.brian.core.events.async.AsyncEvent;
import org.brian.core.events.async.EventData;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Supplier;

public interface AsyncEvents extends Listener, EventExecutor {

    List<AsyncEvents> registeredEvents = new ArrayList<>();

    static <T extends Event> AsyncEvents listen(
            Class<T> type,
            Consumer<T> listener
    ) { return listen(type, EventPriority.NORMAL, listener); }

    static <T extends Event> AsyncEvents listen(
            Class<T> type,
            EventPriority priority,
            Consumer<T> listener
    ) {

        final AsyncEvents events = ($, event) -> {

            if(type.isInstance(event)) {
                async(() -> listener.accept(type.cast(event)));
            }

        };
        Bukkit.getPluginManager().registerEvent(type, events, priority, events, Core.getInstance().getPlugin());
        registeredEvents.add(events);
        return events;
    }

    static <T extends Event> AsyncEvents listen(
            Class<T> type,
            Consumer<T> listener,
            Consumer<T> preAsync
    ) {

        final AsyncEvents events = ($, event) -> {
            if(type.isInstance(event)) {
                preAsync.accept(type.cast(event));
                async(() -> listener.accept(type.cast(event)));
            }
        };
        Bukkit.getPluginManager().registerEvent(type, events, EventPriority.NORMAL, events, Core.getInstance().getPlugin());
        registeredEvents.add(events);
        return events;
    }

    static <T extends Event> AsyncEvents listen(
            AsyncEvent<T> asyncEvent
    ) {

        final AsyncEvents events = ($, event) -> {
            if(asyncEvent.getClassType().isInstance(event)) {

                EventData data = new EventData();
                if(EventData.getDataTypes().containsKey(asyncEvent.getClassType())) EventData.getDataType(asyncEvent.getClassType()).accept(asyncEvent.getClassType().cast(event), data);

                if(asyncEvent.getPreAsync() != null) asyncEvent.getPreAsync().accept(asyncEvent.getClassType().cast(event), data);

                async(() -> {
                    if(asyncEvent.getAsync() != null) asyncEvent.getAsync().accept(asyncEvent.getClassType().cast(event), data);
                });

            }
        };
        if(asyncEvent.getEventPriority() != null) Bukkit.getPluginManager().registerEvent(asyncEvent.getClassType(), events, asyncEvent.getEventPriority(), events, Core.getInstance().getPlugin());
        else Bukkit.getPluginManager().registerEvent(asyncEvent.getClassType(), events, asyncEvent.getEventPriority(), events, Core.getInstance().getPlugin());
        registeredEvents.add(events);
        return events;
    }

    static <T> T gatherFromSync(Supplier<T> supplier) {

        CompletableFuture<T> callback = new CompletableFuture<>();
        new BukkitRunnable(){
            @Override
            public void run() {
                callback.complete(supplier.get());
            }
        }.runTask(Core.getInstance().getPlugin());

        try {
            return callback.get();
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return null;

    }

    static ETask async(Runnable runnable){

        return Energiser.getInstance().runTaskAsync(runnable, false);

    }

    static void unregister(){
        registeredEvents.forEach(HandlerList::unregisterAll);
    }

}
