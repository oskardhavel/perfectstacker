package org.brian.core.events.async;

import org.brian.core.utils.Storagable;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class EventData extends Storagable {

    private static boolean initialized = false;

    private static Map<Class<? extends Event>, BiConsumer<? extends Event, EventData>> dataTypes = new HashMap<>();

    public static <T extends Event> void registerType(Class<T> classz, BiConsumer<T, EventData> biConsumer) {
        dataTypes.put(classz, biConsumer);
    }

    static {
        if(!initialized){
            initialize();
            initialized = true;
        }
    }

    private static void initialize(){

        registerType(BlockBreakEvent.class, (event, eventData) -> {
            eventData.getData().put("blockType", event.getBlock().getType());
        });

    }

    public static <T extends Event> BiConsumer<T, EventData> getDataType(Class<T> type){

        return (BiConsumer<T, EventData>) getDataTypes().get(type);

    }

    public static Map<Class<? extends Event>, BiConsumer<? extends Event, EventData>> getDataTypes() {
        return dataTypes;
    }

}
