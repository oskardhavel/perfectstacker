package org.brian.core.command;

import org.brian.core.command.args.Argument;

import java.util.Map;

public class Command {

    private Map<String, Argument> args;
    private Sender sender;
    public Command(Map<String, Argument> args, Sender sender){

        this.args = args;
        this.sender = sender;

    }

    public Sender getSender() {
        return sender;
    }

    public Map<String, Argument> args() {
        return args;
    }
}
