package org.brian.core.command;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.brian.core.command.args.Argument;
import org.brian.core.command.args.ArgumentType;
import org.brian.core.helper.Helper;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.*;

public class CommandController implements Helper {

    private JavaPlugin plugin;
    private Map<String, CommandInfo> commandMap = new HashMap<>();
    private CommandMap bukkitCommandMap;

    private String mainColor = "&e";
    private String secondColor = "&3";
    private String markupColor = "&7";

    public CommandController(JavaPlugin plugin){
        this.plugin = plugin;
        try {

            Field cMap = SimplePluginManager.class.getDeclaredField("commandMap");
            cMap.setAccessible(true);
            this.bukkitCommandMap = (CommandMap) cMap.get(Bukkit.getPluginManager());

        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void registerCommand(CommandInfo info){
        try {
            bukkitCommandMap.register(plugin.getDescription().getName(), new org.bukkit.command.Command(info.getLabel(), "", "", info.getAliases()) {
                @Override
                public boolean execute(CommandSender sender1, String commandName, String[] args) {

                    Sender sender = new Sender(sender1);

                    if(!info.getPermission().equalsIgnoreCase("none") && sender.isPlayer() && !sender.getPlayer().hasPermission(info.getPermission())) {

                        sender.sendMessage(info.getNoPermissionMessage());
                        return true;

                    }

                    if(args.length == 0 && !info.isSendCommandsOnhelp() && !info.getSubCommands().isEmpty()){

                        if(info.getSubCommands().isEmpty()){

                            String required = "";
                            if(!info.getRequiredArgs().isEmpty()){
                                required = color(buildFormat(info.getRequiredArgs().keySet(), "[", "]", mainColor));
                            }

                            String optional = "";
                            if(!info.getOptionalArgs().isEmpty()){
                                optional = color(buildFormat(info.getOptionalArgs().keySet(), "{", "}", secondColor));
                            }

                            sender.sendMessage("$second: [] - Required, {} - Optional");
                            String properUsage = "&7Proper command usage: /" + info.getLabel() + (required.equalsIgnoreCase("") ? "" : " " + required) + (optional.equalsIgnoreCase("") ? "" : " " + optional) + " $main- $markup" + info.getDescription();

                            sender.sendMessage(color(properUsage));

                        } else {

                            sender.sendMessage("$markup&m&l------------------".replace("$main", mainColor).replace("$markup", markupColor).replace("$second", secondColor));
                            sender.sendMessage("$second[] - Required, {} - Optional".replace("$main", mainColor).replace("$markup", markupColor).replace("$second", secondColor));
                            sender.sendMessage("");
                            sender.sendMessage(("$markupSub Commands for $second/" + commandName).replace("$main", mainColor).replace("$markup", markupColor).replace("$second", secondColor));
                            for (CommandInfo subCommand : info.getSubCommands().values()) {

                                if(!subCommand.getPermission().equalsIgnoreCase("none") && sender.isPlayer() && !sender.getPlayer().hasPermission(subCommand.getPermission())) continue;

                                String required = "";
                                if(!subCommand.getRequiredArgs().isEmpty()){
                                    required = color(buildFormat(subCommand.getRequiredArgs().keySet(), "[", "]", mainColor));
                                }

                                String optional = "";
                                if(!subCommand.getOptionalArgs().isEmpty()){
                                    optional = color(buildFormat(subCommand.getOptionalArgs().keySet(), "{", "}", secondColor));
                                }

                                String usage = ("$main&l- $markup" + subCommand.getLabel() + (required.equalsIgnoreCase("") ? "" : " " + required) + (optional.equalsIgnoreCase("") ? "" : " " + optional) + " $main- $markup" + subCommand.getDescription()).replace("$main", mainColor).replace("$markup", markupColor).replace("$second", secondColor);
                                sender.sendMessage(usage);

                            }
                            sender.sendMessage("");
                            sender.sendMessage("$markup&m&l------------------".replace("$main", mainColor).replace("$markup", markupColor).replace("$second", secondColor));

                        }
                    } else {

                        if(!info.getSubCommands().isEmpty()){

                            CommandInfo subCommand = info.getSubCommands().get(args[0]);

                            if(subCommand == null){

                                sender.sendMessage("&c&l(!)&7 Unknown sub command /" + args[0]);
                                return true;

                            }

                            if(!subCommand.getPermission().equalsIgnoreCase("none") && sender.isPlayer() && !sender.getPlayer().hasPermission(subCommand.getPermission())) {

                                sender.sendMessage(subCommand.getNoPermissionMessage());
                                return true;

                            }

                            int requiredArgsCount = subCommand.getRequiredArgs().size();

                            if(ArrayUtils.remove(args, 0).length >= requiredArgsCount){

                                //Run Checks for Argument Types
                                int currentArg = 1;
                                Map<String, Argument> arguments = new HashMap<>();

                                for(String argumentName : subCommand.getRequiredArgs().keySet()){

                                    ArgumentType argType = subCommand.getRequiredArgs().get(argumentName);
                                    if(!argType.getCheck().test(args[currentArg])){
                                        sender.sendMessage(argType.getCheckFailedMessage().replace("$arg", args[currentArg]));
                                        return true;
                                    }

                                    arguments.put(argumentName, new Argument(argType, args[currentArg]));
                                    currentArg++;

                                }

                                if(args.length > (requiredArgsCount + subCommand.getOptionalArgs().size())){

                                    for(String argumentName : subCommand.getOptionalArgs().keySet()){

                                        ArgumentType argType = subCommand.getOptionalArgs().get(argumentName);
                                        if(!argType.getCheck().test(args[currentArg])){
                                            sender.sendMessage(argType.getCheckFailedMessage().replace("$arg", args[currentArg]));
                                            return true;
                                        }

                                        arguments.put(argumentName, new Argument(argType, args[currentArg]));
                                        currentArg++;

                                    }

                                }

                                subCommand.getListener().accept(new Command(arguments, sender));

                            } else {

                                String required = "";
                                if (!subCommand.getRequiredArgs().isEmpty()) {
                                    required = color(buildFormat(subCommand.getRequiredArgs().keySet(), "[", "]", mainColor));
                                }

                                String optional = "";
                                if (!subCommand.getOptionalArgs().isEmpty()) {
                                    optional = color(buildFormat(subCommand.getOptionalArgs().keySet(), "{", "}", secondColor));
                                }

                                String usage = ("$markupProper Command Usage /" + commandName + " " + subCommand.getLabel() + (required.equalsIgnoreCase("") ? "" : " " + required) + (optional.equalsIgnoreCase("") ? "" : " " + optional)).replace("$main", mainColor).replace("$markup", markupColor).replace("$second", secondColor);
                                sender.sendMessage(usage);

                            }

                        } else {
                            Map<String, Argument> arguments = new HashMap<>();
                            info.getListener().accept(new Command(arguments, sender));
                        }

                    }

                    return true;
                }

                @Override
                public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {

                    List<String> completion = new ArrayList<>();

                    if(args.length == 1){
                        completion.addAll(info.getSubCommands().keySet());
                    } else {

                        CommandInfo subCommand = info.getSubCommands().get(args[0]);
                        if(subCommand == null) {
                            completion.add("Invalid Sub Command.");
                            return completion;
                        }

                        String[] argsWithoutMain = (String[]) ArrayUtils.remove(args, 0);

                        if(subCommand.getTabCompletions().containsKey(argsWithoutMain.length)){
                            subCommand.getTabCompletions().get(argsWithoutMain.length).accept(completion, argsWithoutMain);
                        }

                    }
                    return completion;
                }
            });
            commandMap.put(info.getLabel(), info);
        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public String buildFormat(Set<String> list, String openBrackets, String closeBrackets, String color){

        //[arg], [args], [args]
        if(list.isEmpty()) return "";
        return ("$color" + openBrackets + StringUtils.join(new ArrayList<>(list), closeBrackets + "&7 $color" + openBrackets) + closeBrackets).replace("$color", color);

    }

    public void unregisterCommand(String commandName){

        org.bukkit.command.Command command = bukkitCommandMap.getCommand(commandName);
        if(command == null) return;

        command.unregister(bukkitCommandMap);

    }

    public void setMainColor(String mainColor) {
        this.mainColor = mainColor;
    }

    public void setMarkupColor(String markupColor) {
        this.markupColor = markupColor;
    }

    public void setSecondColor(String secondColor) {
        this.secondColor = secondColor;
    }
}
