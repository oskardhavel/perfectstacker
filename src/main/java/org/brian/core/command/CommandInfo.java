package org.brian.core.command;


import org.brian.core.command.args.ArgumentType;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class CommandInfo {

    private String label;
    private Map<String, ArgumentType> requiredArgs = new LinkedHashMap<>();
    private Map<String, ArgumentType> optionalArgs = new LinkedHashMap<>();
    private Map<Integer, BiConsumer<List<String>, String[]>> tabCompletions = new HashMap<>();
    private String permission = "none";
    private String noPermissionMessage = "&cYou don't have permission!";
    private boolean sendCommandsOnhelp = false;
    private String description;
    private List<String> aliases = new ArrayList<>();
    private Consumer<Command> listener;
    private Map<String, CommandInfo> subCommands = new HashMap<>();

    public String getLabel() {
        return label;
    }

    public Map<String, ArgumentType> getOptionalArgs() {
        return optionalArgs;
    }

    public Map<String, ArgumentType> getRequiredArgs() {
        return requiredArgs;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public Consumer<Command> getListener() {
        return listener;
    }

    public Map<String, CommandInfo> getSubCommands() {
        return subCommands;
    }

    public void addSubCommand(CommandInfo info){
        subCommands.put(info.getLabel(), info);
    }

    public boolean isSendCommandsOnhelp() {
        return sendCommandsOnhelp;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setListener(Consumer<Command> listener) {
        this.listener = listener;
    }

    public void setSubCommands(Map<String, CommandInfo> subCommands) {
        this.subCommands = subCommands;
    }

    public void setSendCommandsOnhelp(boolean sendCommandsOnhelp) {
        this.sendCommandsOnhelp = sendCommandsOnhelp;
    }

    public void addRequiredArgument(String arg, ArgumentType type){
        this.requiredArgs.put(arg, type);
    }

    public void addOptionalArgument(String arg, ArgumentType type){
        this.optionalArgs.put(arg, type);
    }

    public void addAliase(String label){
        this.aliases.add(label);
    }

    public void registerTabCompletion(Integer arg, BiConsumer<List<String>, String[]> biConsumer){
        tabCompletions.put(arg, biConsumer);
    }

    public Map<Integer, BiConsumer<List<String>, String[]>> getTabCompletions() {
        return tabCompletions;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public void setNoPermissionMessage(String noPermissionMessage) {
        this.noPermissionMessage = noPermissionMessage;
    }

    public String getNoPermissionMessage() {
        return noPermissionMessage;
    }

    public String getPermission() {
        return permission;
    }
}
