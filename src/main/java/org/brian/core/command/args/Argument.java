package org.brian.core.command.args;

public class Argument {

    private ArgumentType argType;
    private String argValue;

    public Argument(ArgumentType type, String argValue){

        this.argType = type;
        this.argValue = argValue;

    }

    public ArgumentType getArgType() {
        return argType;
    }

    public String getArgValue() {
        return argValue;
    }
}
