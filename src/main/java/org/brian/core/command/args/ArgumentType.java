package org.brian.core.command.args;

import java.util.function.Predicate;

public class ArgumentType {

    private Predicate<String> check;
    private String checkFailedMessage;

    public ArgumentType(){}

    public ArgumentType setCheck(Predicate<String> check) {
        this.check = check;
        return this;
    }

    public ArgumentType setCheckFailedMessage(String checkFailedMessage) {
        this.checkFailedMessage = checkFailedMessage;
        return this;
    }

    public Predicate<String> getCheck() {
        return check;
    }

    public String getCheckFailedMessage() {
        return checkFailedMessage;
    }
}
