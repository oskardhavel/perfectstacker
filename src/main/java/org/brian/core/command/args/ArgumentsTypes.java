package org.brian.core.command.args;

import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Bukkit;

import java.util.HashMap;
import java.util.Map;

public class ArgumentsTypes {

    private static Map<String, ArgumentType> argumentsTypes = new HashMap<>();
    private static boolean inited = false;

    static {
        init();
    }

    private static void init(){
        if(!inited){

            addNew("int", new ArgumentType().setCheck(NumberUtils::isNumber).setCheckFailedMessage("&c&l(!)&7 Failed to parse &c$arg&7 argument as number!"));

            addNew("player", new ArgumentType().setCheck((string) -> Bukkit.getPlayer(string) != null).setCheckFailedMessage("&c&l(!)&7 Failed to find a player by name &c$arg"));

            addNew("string", new ArgumentType());

        }
    }

    public static void addNew(String typeName, ArgumentType argumentType){
        argumentsTypes.put(typeName, argumentType);
    }

    public static boolean containsArgumentType(String type){
        return argumentsTypes.containsKey(type);
    }

    public static ArgumentType getType(String type){
        return argumentsTypes.get(type);
    }

}
