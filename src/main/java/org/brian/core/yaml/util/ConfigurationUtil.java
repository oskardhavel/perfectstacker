package org.brian.core.yaml.util;

import org.apache.commons.math3.util.Pair;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.mapper.ObjectsMapper;
import org.brian.core.yaml.value.ConfigurationList;
import org.brian.core.yaml.value.ConfigurationValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class ConfigurationUtil {

    public static String stringWithSpaces(int spaces){
        StringBuilder builder = new StringBuilder();
        IntStream.range(0, spaces).forEach(s -> builder.append(" "));
        return builder.toString();
    }

    public static int findSectionEnd(int sectionStart, OOPIterator<UnreadString> iterator){

        UnreadString[] array = iterator.getObjectsCopy(UnreadString.class);

        int startingSpaces = -1;
        int lastIndex = 0;

        for(int index = sectionStart; index < array.length; index++){

            Pair<String, Integer> line = parse(array[index].value());
            if(startingSpaces == -1) {
                startingSpaces = line.getValue();
                continue;
            }

            if(line.getValue() <= startingSpaces && line.getKey().replaceAll("\\s+", "").length() > 0) return index-1;

            lastIndex = index;

        }

        return lastIndex;

    }

    public static ConfigurationSection loadSection(OOPIterator<UnreadString> iterator){

        ConfigurationSection leadSection = null;
        ConfigurationSection currentSection = null;

        List<String> description = new ArrayList<>();

        while(iterator.hasNext()){

            UnreadString line = iterator.next();
            if(line == null) continue;
            if(line.value().contains(":") && !line.value().contains("#")){

                String[] split = line.value().split(":");
                if(split.length == 1){

                    //Now we have to check if the line below is a list or not
                    UnreadString[] valuesArray = iterator.getObjectsCopy(UnreadString.class);
                    int elementIndex = Arrays.asList(valuesArray).indexOf(line);
                    boolean valid = isValidIndex(valuesArray, elementIndex+1);

                    if(valid && isList(valuesArray, elementIndex+1)){

                        //IS LIST
                        List<UnreadString> listValues = iterator.nextValuesThatMatches(us -> us.value().contains("-"), true);
                        Pair<String, Integer> parsedKey = parse(split[0]);
                        ConfigurationList value = new ConfigurationList(parsedKey.getKey(), listValues.stream().map(UnreadString::value).map(string -> parse(parse(string).getKey().substring(1)).getKey()).collect(toList()));

                        value.spaces(parsedKey.getValue());
                        value.description(description);

                        if(currentSection != null) {

                            if (currentSection.spaces() >= value.spaces()) {

                                //We need to find parent that has less spaces and the different is between 1-4
                                ConfigurationSection section = currentSection.allParents().stream().
                                        filter(s -> s.spaces() < value.spaces()).
                                        filter(s -> (value.spaces() - s.spaces()) <= 4).findFirst().orElse(null);

                                if(section != null) {
                                    currentSection = section;
                                    currentSection.assignValue(value);
                                }

                            } else {

                                currentSection.assignValue(value);

                            }
                        }

                    } else {

                        //IS SECTION
                        Pair<String, Integer> pair = parse(split[0]);
                        ConfigurationSection newSection = new ConfigurationSection(pair.getKey(), pair.getValue());
                        newSection.description(description);

                        if(leadSection == null) leadSection = newSection;
                        else currentSection.findAcceptableParent(newSection).assignChild(newSection);

                        currentSection = newSection;

                    }

                } else {

                    Pair<String, Integer> parsedKey = parse(split[0]);
                    ConfigurationValue value = new ConfigurationValue(parsedKey.getKey(), ObjectsMapper.mapObject(parse(split[1]).getKey()));

                    value.spaces(parsedKey.getValue());
                    value.description(description);

                    if(currentSection != null) {

                        if (currentSection.spaces() >= value.spaces()) {

                            //We need to find parent that has less spaces and the different is between 1-4
                            ConfigurationSection section = currentSection.allParents().stream().
                                    filter(s -> s.spaces() < value.spaces()).
                                    filter(s -> (value.spaces() - s.spaces()) <= 4).findFirst().orElse(null);

                            if(section != null) {
                                currentSection = section;
                                currentSection.assignValue(value);
                            }

                        } else {

                            currentSection.assignValue(value);

                        }
                    }

                }

            } else {

                if(line.value().contains("#")){
                    description.add(parse(line.value()).getKey());
                } else if(line.value().trim().length() == 0 && !description.isEmpty()) description.add(line.value());

            }

        }

        return leadSection;

    }

    public static int findSpaces(String string){

        int count = 0;
        for(Character c : string.toCharArray()){

            if(c.toString().equalsIgnoreCase(" ")) count++;
            else return count;

        }

        return count;

    }

    public static <T> List<T> copy(T[] array, int startIndex, int endIndex){

        List<T> list = new ArrayList<>();

        for(int index = startIndex; index < array.length; index++){

            list.add(array[index]);
            if(index == endIndex) break;

        }

        return list;

    }

    public static Pair<String, Integer> parse(String key){

        int spaces = 0;
        StringBuilder builder = new StringBuilder();
        int lcount = 0;

        boolean foundFirstChar = false;

        for(Character c : key.toCharArray()){

            if(c.toString().equalsIgnoreCase(" ") && !foundFirstChar) spaces++;
            else {

                if(lcount == 0 && c.toString().equalsIgnoreCase("\"")) continue;
                if(lcount == key.substring(spaces).length()-2 && c.toString().equalsIgnoreCase("\"")) continue;
                builder.append(c);
                foundFirstChar = true;
                lcount++;
            }

        }

        return new Pair<>(builder.toString(), spaces);

    }

    public static <T> boolean isValidIndex(T[] array, int index){
        return array.length > index;
    }

    public static <T> boolean filter(T[] array, int startingIndex, Predicate<T> filter){

        int valuesFound = 0;

        for(int index = startingIndex; index < array.length; index++){

            T value = array[index];
            if(value.toString().contains("#") || value.toString().trim().length() == 0) continue;

            if(parse(value.toString()).getKey().contains(":")) return false;

            boolean toReturn = filter.test(value);
            if(toReturn && parse(value.toString()).getKey().contains(":")) return false;
            if(toReturn) return true;

        }

        return false;

    }

    public static boolean isList(UnreadString[] array, int startingIndex){

        for(int index = startingIndex; index < array.length; index++){

            UnreadString value = array[index];

            //We have to test the value if it starts with '-' after all spaces is removed.
            //If it starts not with '-' it's not a list, but we must ignore white spaces & comments

            //Checking if string is a white space
            if(value.value().trim().length() == 0) continue;

            //Getting the first character after all the spaces
            String firstChar = firstCharsAfterSpaces(value.toString(), 1);

            //Checking if it's a comment if so continueing
            if(firstChar.equalsIgnoreCase("#")) continue;

            //We have found a list value!
            if(firstChar.equalsIgnoreCase("-")) return true;
            else return false;

        }

        return false;

    }

    public static String firstCharsAfterSpaces(String text, int charsAmount){

        StringBuilder builder = new StringBuilder();
        int charsFound = 0;

        for(Character c : text.toCharArray()){

            if(!c.toString().equalsIgnoreCase(" ")) {
                charsFound++;
                builder.append(c);
                if(charsAmount == charsFound) return builder.toString();
            }

        }

        return "";

    }

    public static boolean containsChar(String line) {

        List<String> chars = new ArrayList<>();
        for(Character c : line.toCharArray()){
            if(c.toString().trim().length() > 0) return true;
        }

        return false;

    }
}
