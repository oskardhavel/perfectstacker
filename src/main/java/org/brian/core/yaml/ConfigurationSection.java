package org.brian.core.yaml;

import org.brian.core.yaml.util.ConfigurationUtil;
import org.brian.core.yaml.util.Descriptionable;
import org.brian.core.yaml.value.AConfigurationValue;
import org.brian.core.yaml.value.ConfigurationList;
import org.brian.core.yaml.value.ConfigurationValue;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ConfigurationSection extends Descriptionable {

    private String key;
    private ConfigurationSection parent;
    private Map<String, AConfigurationValue> values = new HashMap<>();
    private Map<String, ConfigurationSection> childs = new HashMap<>();
    private int spaces;
    private int line = -1;

    public ConfigurationSection(String key, int spaces){
        this.key = new String(key);
        this.spaces = spaces;
    }

    public String key() {
        return key;
    }

    public ConfigurationSection parent() {
        return parent;
    }

    public ConfigurationSection parent(ConfigurationSection parent){
        parent.childs.put(key(), this);
        this.parent = parent;
        return this;
    }

    public Map<String, AConfigurationValue> values(){
        return values;
    }

    public Map<String, Object> values(boolean converted){

        Map<String, Object> values = new HashMap<>();
        values().forEach((k, v) -> values.put(k, v.value()));
        return values;

    }

    public int spaces(){
        return spaces;
    }

    public Map<String, ConfigurationSection> childs() {
        return childs;
    }

    public void assignChild(ConfigurationSection section){
        section.parent(this);
        childs.put(section.key(), section);
    }

    public void assignValue(AConfigurationValue value){
        value.parent(this);
        value.spaces(spaces()+2);
        values.put(value.key(), value);
    }

    public List<ConfigurationSection> allParents(){

        List<ConfigurationSection> parents = new ArrayList<>();
        getParents(parents);
        return parents;

    }

    public List<ConfigurationSection> allChilds(){

        List<ConfigurationSection> childs = new ArrayList<>();
        childs.addAll(childs().values());

        childs().values().forEach(c -> childs.addAll(c.allChilds()));

        return childs;

    }

    private void getParents(List<ConfigurationSection> parents){

        parents.add(this);
        if(parent() != null) parent().getParents(parents);

    }

    public ConfigurationSection mainParent() {
        if(parent == null) return this;
        else return parent.mainParent();
    }

    public Object value(String path, Object ifNotPresent){

        if(!path.contains(".")) {

            if(values.containsKey(path)) return values.get(path).value();
            else return ifNotPresent;

        } else {

            String split[] = path.split("\\.");
            String valueKey = split[split.length - 1];

            ConfigurationSection section = null;

            for (int index = 0; index < split.length - 1; index++) {

                String key = split[index];
                if (section == null) section = childs.get(key);
                else section = section.childs().get(key);

            }

            if (section != null) return section.values().get(valueKey).value();
        }

        return ifNotPresent;

    }

    public Object value(String path){
        return value(path, null);
    }

    public <T> T value(String path, Class<T> type, T ifNotPresent){
        return type.cast(value(path, ifNotPresent));
    }

    public <T> T valueAsRequired(String path){
        return (T) value(path, null);
    }

    public <T> T valueAsRequired(String path, Object ifNotPresent){
        return (T)value(path, ifNotPresent);
    }

    public void write(BufferedWriter bw) throws IOException {

        bw.newLine();
        bw.write(ConfigurationUtil.stringWithSpaces(spaces()) + key() + ":");

        for(AConfigurationValue value : values.values().stream().filter(o -> o instanceof ConfigurationValue).collect(Collectors.toList())) {

            bw.newLine();
            if(!value.description().isEmpty()) System.out.println("KEY: "+ value.key() + ", desc: " + value.description());
            value.writeDescription(bw, value.spaces());
            value.write(bw);

        }

        for(AConfigurationValue value : values.values().stream().filter(o -> o instanceof ConfigurationList).collect(Collectors.toList())) {

            bw.newLine();
            if(!value.description().isEmpty()) System.out.println("KEY: "+ value.key() + ", desc: " + value.description());
            value.writeDescription(bw, value.spaces());
            value.write(bw);

        }

        for (ConfigurationSection value : childs.values()) {
            bw.newLine();
            value.write(bw);
        }

    }

    public String path() {

        List<ConfigurationSection> parents = allParents();
        Collections.reverse(parents);

        StringBuilder builder = new StringBuilder();
        int count = 0;

        for(ConfigurationSection parent : parents){

            if(count != (parents.size()-1))
                builder.append(parent.key()).append(".");
            else
                builder.append(parent.key());

            count++;
        }

        return builder.toString();

    }

    public Map<String, AConfigurationValue> allValues() {

         Map<String, AConfigurationValue> allValues = new HashMap<>();
         allValues(allValues);

         return allValues;
    }
    private void allValues(Map<String, AConfigurationValue> allValues){

        values.forEach((k, v) -> allValues.put(v.path(), v));
        childs.values().forEach(c -> c.allValues(allValues));

    }

    public boolean isPresentValue(String path){
        return value(path, null) != null;
    }

    public boolean isPresentChild(String child){
        return childs().containsKey(child);
    }

    public ConfigurationSection child(String sectionName) {

        if(!sectionName.contains(".")) {

            return childs().get(sectionName);

        } else {

            String split[] = sectionName.split("\\.");

            ConfigurationSection section = null;

            for (int index = 0; index < split.length - 1; index++) {

                String key = split[index];
                if (section == null) section = childs.get(key);
                else section = section.childs().get(key);

            }

            return section;
        }

    }

    public ConfigurationSection findAcceptableParent(ConfigurationSection newSection) {

        //Check one if newSection and this section spaces are equal
        if(newSection.spaces == spaces()) return parent();

        //If this section spaces are more than newSection spaces
        if(newSection.spaces() < spaces()) return parent().findAcceptableParent(newSection);

        return this;

    }
}
