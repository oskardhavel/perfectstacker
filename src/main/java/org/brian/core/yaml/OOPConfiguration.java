package org.brian.core.yaml;

import org.apache.commons.math3.util.Pair;
import org.brian.core.yaml.util.ConfigurationUtil;
import org.brian.core.yaml.util.OOPIterator;
import org.brian.core.yaml.util.UnreadString;
import org.brian.core.yaml.value.AConfigurationValue;

import java.io.*;
import java.util.*;

import static org.brian.core.yaml.util.ConfigurationUtil.filter;
import static org.brian.core.yaml.util.ConfigurationUtil.isValidIndex;

public class OOPConfiguration {

    private File file;
    private Map<String, AConfigurationValue> values = new HashMap<>();
    private Map<String, ConfigurationSection> sections = new HashMap<>();

    public OOPConfiguration(File file){
        try{

            this.file = file;

            BufferedReader reader = new BufferedReader(new FileReader(file));
            String sCurrentLine;
            int index = 0;
            List<UnreadString> lines = new ArrayList<>();

            while ((sCurrentLine = reader.readLine()) != null) {

                UnreadString unreadString = new UnreadString(index, sCurrentLine);
                lines.add(unreadString);
                index++;

            }

            reader.close();
            UnreadString array[] = new UnreadString[lines.size()];
            lines.forEach(line -> array[line.index()] = line);

            ConfigurationSection key = null;

            OOPIterator<UnreadString> looper = new OOPIterator<>(array);
            List<UnreadString> mainSections = new ArrayList<>();

            while(looper.hasNext()){

                UnreadString line = looper.next();
                if(line != null) {
                    if (line.value().contains(":")) {

                        String split[] = line.value().split(":");
                        if (split.length == 1) {

                            UnreadString[] valuesArray = looper.getObjectsCopy(UnreadString.class);
                            int elementIndex = Arrays.asList(valuesArray).indexOf(line);
                            boolean valid = isValidIndex(valuesArray, elementIndex+1);

                            if(valid && !filter(valuesArray, elementIndex+1, us -> ((Character) ConfigurationUtil.parse(us.value()).getKey().charAt(0)).toString().equals("-")) && ConfigurationUtil.parse(split[0]).getValue() == 0) {
                                mainSections.add(line);
                            }

                        }
                    }
                }

            }

            for(UnreadString headSection : mainSections){

                int startingIndex = Arrays.asList(array).indexOf(headSection);
                int endIndex = ConfigurationUtil.findSectionEnd(startingIndex, looper);
                ConfigurationSection section = ConfigurationUtil.loadSection(new OOPIterator(ConfigurationUtil.copy(array, startingIndex, endIndex)));

                sections.put(section.key(), section);

            }

            if(key != null && key.parent() == null) sections.put(key.key(), key);

        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public AConfigurationValue value(String path){

        if(!path.contains(".")) {

            if(values.containsKey(path)) return values.get(path);
            else return null;

        } else {

            String split[] = path.split("\\.");
            String valueKey = split[split.length-1];

            ConfigurationSection section = null;

            for(int index = 0; index < split.length-1; index++){

                String key = split[index];
                if(section == null) section = sections.get(key);
                else section = section.childs().get(key);

            }

            if(section != null && section.values().containsKey(valueKey)) return section.values().get(valueKey);

        }

        return null;

    }

    public <T> T value(String path, Class<T> type){

        if(!path.contains(".")) {

            if(values.containsKey(path)) return type.cast(values.get(path).value());
            else return null;

        } else {

            String split[] = path.split("\\.");
            String valueKey = split[split.length-1];

            ConfigurationSection section = null;

            for(int index = 0; index < split.length-1; index++){

                String key = split[index];
                if(section == null) section = sections.get(key);
                else section = section.childs().get(key);

            }

            if(section != null && section.values().containsKey(valueKey)) return type.cast(section.values().get(valueKey).value());

        }

        return null;

    }

    public Pair<ConfigurationSection, List<ConfigurationSection>> section(String path){

        List<ConfigurationSection> sections = new ArrayList<>();

        if(!path.contains(".")){
            if(sections().containsKey(path)) sections.addAll(sections().get(path).childs().values());
            return new Pair<>(sections().get(path), sections);
        } else{

            String split[] = path.split("\\.");
            ConfigurationSection section = null;

            for(int index = 0; index < split.length; index++){

                String key = split[index];
                if(section == null) section = sections().get(key);
                else section = section.childs().get(key);

            }

            if(section != null) {
                sections.addAll(section.allParents());
                return new Pair<>(section, sections);
            }
        }

        return null;

    }

    public AConfigurationValue set(String path, Object object){

        AConfigurationValue value = null;
        if(object instanceof AConfigurationValue){
            value = (AConfigurationValue)object;
        }

        if(!path.contains(".")){
            if(value == null) value = AConfigurationValue.fromObject(path, object);
            values.put(path, value);
            return value;
        } else {

            String split[] = path.split("\\.");
            ConfigurationSection section = null;
            if(value == null) value = AConfigurationValue.fromObject(split[split.length-1], object);
            int currentSpaces = 0;

            for(int index = 0; index < split.length-1; index++){

                String key = split[index];
                if(section == null) {
                    if(sections.containsKey(key)){
                        section = sections.get(key);
                    } else {
                        section = new ConfigurationSection(key, currentSpaces);
                        sections.put(key, section);
                    }
                }
                else {
                    if(section.childs().containsKey(key)) section = section.childs().get(key);
                    else {
                        ConfigurationSection section2 = new ConfigurationSection(key, section.spaces());
                        section.assignChild(section2);
                        section = section2;
                    }
                }

                currentSpaces += 2;

            }

            if(section != null) {
                section.values().put(split[split.length-1], value);
                value.spaces(value.spaces() <= 0 ? section.spaces()+2 : value.spaces());
                value.parent(section);
            }
            return value;

        }
    }

    public Map<String, AConfigurationValue> values() {
        return values;
    }

    public Map<String, AConfigurationValue> allValues(){

        Map<String, AConfigurationValue> allValues = new HashMap<>();

        values.forEach((k, v) -> allValues.put(v.path(), v));
        for(ConfigurationSection section : sections.values()){
            allValues.putAll(section.allValues());
        }

        return allValues;

    }

    public List<ConfigurationSection> allSections(){

        List<ConfigurationSection> sections = new ArrayList<>();
        sections().values().forEach(section -> sections.addAll(section.allChilds()));

        return sections;

    }

    public Map<String, ConfigurationSection> sections() {
        return sections;
    }

    public void save() {
        FileWriter w = null;
        BufferedWriter bw = null;

        try {
            File copyFile = new File(file.getParent() + "/testCopy.yml");
            if (!copyFile.exists()) file.createNewFile();

            w = new FileWriter(copyFile);
            bw = new BufferedWriter(w);

            bw.newLine();
            bw.write("#Configuration made with OOPCore!");
            bw.newLine();
            bw.write("#An awesome library written by Brian / OOP-778");
            bw.newLine();

            for (AConfigurationValue value : values().values()) {

                bw.newLine();
                value.writeDescription(bw, value.spaces());
                value.write(bw);

            }

            for(ConfigurationSection section : sections.values()){

                bw.newLine();
                section.writeDescription(bw, section.spaces());
                section.write(bw);

            }

        } catch (Exception ex){
            ex.printStackTrace();
        } finally {
            try{
                if(bw != null) bw.close();
                if(w != null) w.close();
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }

    public File file(){
        return file;
    }
    public OOPConfiguration file(File file){
        this.file = file;
        return this;
    }

}
