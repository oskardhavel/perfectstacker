package org.brian.core.yaml;

import com.google.common.collect.HashBiMap;
import org.brian.core.yaml.util.ConfigurationUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class LineAssigner {

    private OOPConfiguration configuration;
    private List<Integer> assignedLines = new ArrayList<>();

    public LineAssigner(OOPConfiguration configuration){
        this.configuration = configuration;
    }

    public void assign(){

        //Load all the lines
        File configurationFile = configuration.file();
        if(!configurationFile.exists()) return;

        HashBiMap<Integer, String> fileLines = HashBiMap.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(configurationFile));
            String sCurrentLine;
            int line = 1;

            while ((sCurrentLine = reader.readLine()) != null) {

                if(ConfigurationUtil.containsChar(sCurrentLine) && !sCurrentLine.contains("#")) fileLines.put(line, sCurrentLine);
                line++;

            }

            reader.close();

        } catch (Exception ex){
            ex.printStackTrace();
        }

        //Start off with assigning lines for sections
        List<ConfigurationSection> sectionList = configuration.allSections();

    }

}
