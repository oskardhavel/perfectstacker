package org.brian.core.yaml.value;

import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.mapper.ObjectsMapper;
import org.brian.core.yaml.util.ConfigurationUtil;

import java.io.BufferedWriter;
import java.io.IOException;

public class ConfigurationValue extends AConfigurationValue {

    private Object value;

    public ConfigurationValue(String key, Object value){
        super(key);
        this.value = value;
    }

    public ConfigurationValue(String key, Object value, ConfigurationSection parent){
        super(key, parent);
        this.value = value;
    }


    @Override
    public Object value() {
        return value;
    }

    @Override
    public void write(BufferedWriter bw) throws IOException {

        bw.write(ConfigurationUtil.stringWithSpaces(spaces()) + key() + ": " + ObjectsMapper.toString(value()));

    }
}
