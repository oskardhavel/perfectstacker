package org.brian.core.message;

import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.brian.core.message.addition.AMessageAddition;
import org.brian.core.message.addition.CommandAddition;
import org.brian.core.utils.Assert;
import org.brian.core.utils.MiscUtils;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.value.AConfigurationValue;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OOPMessage {

    private List<MessageLine> messages = new ArrayList<>();
    private boolean centered = false;

    public List<MessageLine> getMessages() {
        return messages;
    }

    public OOPMessage addLine(MessageLine line){
        messages.add(line);
        return this;
    }

    public OOPMessage addLine(String content){
        messages.add(new MessageLine(content));
        return this;
    }

    public void send(Player player){

        for(MessageLine line : messages){

            String lineContent = centered ? Centered.getCenteredMessage(line.getContent()) : line.getContent();

            TextComponent textComponent = new TextComponent(lineContent);
            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(line.getHoverText()).create()));

            line.getAdditionList().forEach(addition -> addition.apply(textComponent));

            player.spigot().sendMessage(textComponent);
            line.getAdditionList().forEach(AMessageAddition::start);

        }

    }

    public OOPMessage center(boolean center){
        this.centered = center;
        return this;
    }

    public OOPMessage replace(Map<String, String> replace){

        String[] toReplace = new String[replace.size()];
        String[] replacement = new String[replace.size()];

        messages.forEach(m -> {
            m.replaceInContent(toReplace, replacement);
            m.replaceInHover(toReplace, replacement);
        });

        return this;
    }

    public static OOPMessage fromConfig(ConfigurationSection section){

        OOPMessage oopMessage = new OOPMessage();
        Map<Integer, MessageLine> lines = new HashMap<>();

        String content = "";
        String hoverText = "";

        for(ConfigurationSection lineSection : section.childs().values()) {

            Assert.assertTrue("Section key should be a line number!", MiscUtils.isInteger(lineSection.key()));
            Assert.assertFalse("Content not found in section: " + lineSection.key(), section.isPresentValue("content"));

            content = lineSection.valueAsRequired("content");
            hoverText = lineSection.isPresentValue("hover text") ? lineSection.valueAsRequired("hover text") : "";

            MessageLine line = new MessageLine(content, hoverText);

            if(lineSection.isPresentChild("additions")){

                ConfigurationSection additionsSection = lineSection.child("additions");

                //Check for RUN_COMMAND addition
                if(additionsSection.isPresentValue("RUN_COMMAND")) {

                    Assert.assertTrue("Value for addition: RUN_COMMAND not found!", additionsSection.isPresentValue("value"));
                    CommandAddition commandAddition = new CommandAddition(additionsSection.valueAsRequired("value"));
                    line.addAddition(commandAddition);

                }

            }

            lines.put(Integer.parseInt(lineSection.key()), line);

        }

        for(AConfigurationValue lineValue : section.values().values()){

            Assert.assertTrue("Value key should be a line number!", MiscUtils.isInteger(lineValue.key()));
            lines.put(Integer.parseInt(lineValue.key()), new MessageLine(lineValue.valueAsRequired()));

        }

        return oopMessage;

    }

}
