package org.brian.core.message.addition;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class CommandAddition extends AMessageAddition {

    private String cmd;
    public CommandAddition(String command){
        this.cmd = command;
    }

    @Override
    public void apply(TextComponent textComponent) {
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd));
    }

    @Override
    public void start() {
        //Not needed
    }
}
