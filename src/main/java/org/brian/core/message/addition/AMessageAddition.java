package org.brian.core.message.addition;

import net.md_5.bungee.api.chat.TextComponent;

public abstract class AMessageAddition {

    public abstract void apply(TextComponent textComponent);
    public abstract void start();

}
