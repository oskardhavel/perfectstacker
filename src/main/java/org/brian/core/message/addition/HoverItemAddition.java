package org.brian.core.message.addition;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.brian.core.Core;
import org.brian.core.utils.ItemStackUtil;
import org.bukkit.inventory.ItemStack;

public class HoverItemAddition extends AMessageAddition {

    private ItemStack toShow;

    public HoverItemAddition() {
        if(Core.getInstance() == null) throw new IllegalStateException("Tried to access " +  getClass().getSimpleName() + " while core is not initialized!");
    }

    public HoverItemAddition show(ItemStack item){
        this.toShow = item;
        return this;
    }

    @Override
    public void apply(TextComponent textComponent) {

        if(toShow != null){

            try {

                BaseComponent[] components = new BaseComponent[]{new TextComponent(ItemStackUtil.itemStackToJson(toShow))};
                textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ITEM, components));

            } catch (Exception ex){
                ex.printStackTrace();
            }

        }

    }


    @Override
    public void start() {
        //Nothing
    }
}
