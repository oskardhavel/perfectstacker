package org.brian.core.message.addition;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.brian.core.Core;
import org.brian.core.command.CommandInfo;
import org.brian.core.energiser.Energiser;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class ClickableConsumerAddition extends AMessageAddition {

    private int timeOut = -1;
    private int maxClickCount = -1;
    private int maxPlayersClickCount = -1;
    private Consumer<Player> runOnClick;
    private boolean isTimeOut;
    private CommandInfo tempCommand;
    private Runnable runAfterTimeOut;
    private Map<Player, Integer> clickedPlayers = new HashMap<>();

    public ClickableConsumerAddition(){
        if(Core.getInstance() == null) throw new IllegalStateException("Tried to access " +  getClass().getSimpleName() + " while core is not initialized!");
    }

    public ClickableConsumerAddition timeOut(Integer timeOut){
        this.timeOut = timeOut;
        return this;
    }

    public ClickableConsumerAddition maxClickCount(Integer maxClickCount){
        this.maxClickCount = maxClickCount;
        return this;
    }

    public ClickableConsumerAddition maxPlayersClickCount(Integer maxPlayersClickCount){
        this.maxPlayersClickCount = maxPlayersClickCount;
        return this;
    }

    public ClickableConsumerAddition onClick(Consumer<Player> runnable){
        this.runOnClick = runnable;
        return this;
    }

    public ClickableConsumerAddition afterTimeOut(Runnable runnable){
        this.runAfterTimeOut = runnable;
        return this;
    }

    public boolean isTimeOut() {
        return isTimeOut;
    }
    public ClickableConsumerAddition timeOut(Boolean isTimeOut){
        this.isTimeOut = isTimeOut;
        return this;
    }

    public Map<Player, Integer> getClickedPlayers() {
        return clickedPlayers;
    }

    void build() {

        if(runOnClick != null){

            this.tempCommand = new CommandInfo();
            String commandLabel = "oopCore-" + UUID.randomUUID().toString();
            tempCommand.setLabel(commandLabel);
            tempCommand.setListener(command -> {

                if(isTimeOut){
                    Core.getInstance().getCommandManager().unregisterCommand(commandLabel);
                }

                Player clickedPlayer = command.getSender().getPlayer();

                if(maxPlayersClickCount != -1 && clickedPlayers.keySet().size() == maxPlayersClickCount){
                    return;
                }

                if(clickedPlayers.containsKey(clickedPlayer)){

                    int clickCount = clickedPlayers.get(clickedPlayer);
                    if(maxClickCount != -1 && clickCount == maxClickCount){
                        return;
                    }
                    clickCount++;
                    clickedPlayers.remove(clickedPlayer);
                    clickedPlayers.put(clickedPlayer, clickCount);

                } else {
                    int clickCount = 1;
                    clickedPlayers.put(clickedPlayer, clickCount);
                }

                runOnClick.accept(clickedPlayer);

            });

        }

    }

    @Override
    public void apply(TextComponent textComponent) {
        build();
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + tempCommand.getLabel()));
    }

    @Override
    public void start() {
        Core.getInstance().getCommandManager().registerCommand(tempCommand);
        if(timeOut != -1){
            Energiser.getInstance().runTaskDelayed(() -> {
                Core.getInstance().getCommandManager().unregisterCommand(tempCommand.getLabel());
                runAfterTimeOut.run();
            }, timeOut, TimeUnit.SECONDS);
        }
    }

    public void refresh(){
        getClickedPlayers().clear();
        timeOut(false);
        build();
    }

}
