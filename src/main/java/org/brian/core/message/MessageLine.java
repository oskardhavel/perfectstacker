package org.brian.core.message;

import org.apache.commons.lang3.StringUtils;
import org.brian.core.message.addition.AMessageAddition;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.ArrayUtils.toArray;

public class MessageLine {

    private String content;
    private String hoverText = "";
    private List<AMessageAddition> additionList = new ArrayList<>();

    public MessageLine(String content, String hoverText){
        this.content = content;
        this.hoverText = hoverText;
    }

    public MessageLine(String content){
        this(content, null);
    }

    public String getContent() {
        return content;
    }

    public MessageLine addAddition(AMessageAddition addition){
        additionList.add(addition);
        return this;
    }

    public MessageLine hoverText(String hoverText) {
        this.hoverText = hoverText;
        return this;
    }

    public MessageLine replaceInContent(String[] toReplace, String[] replacement){

        this.content = StringUtils.replaceEach(this.content, toReplace, replacement);
        return this;

    }

    public MessageLine replaceInHover(String[] toReplace, String[] replacement){

        this.hoverText = StringUtils.replaceEach(this.hoverText, toReplace, replacement);
        return this;

    }

    public MessageLine replaceInHover(String replace, String replacement){
        return replaceInHover(toArray(replace), toArray(replacement));
    }

    public MessageLine replaceInContent(String replace, String replacement){
        return replaceInContent(toArray(replace), toArray(replacement));
    }

    public MessageLine content(String content) {
        this.content = content;
        return this;
    }

    public MessageLine hover(String hoverText){
        this.hoverText = hoverText;
        return this;
    }

    public String getHoverText() {
        return hoverText;
    }

    public List<AMessageAddition> getAdditionList() {
        return additionList;
    }
}
