package org.brian.core.async;

import org.brian.core.energiser.Energiser;
import org.brian.core.helper.Helper;
import org.bukkit.Chunk;
import org.bukkit.entity.Entity;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.function.Predicate;

public class AsyncEntitiesGather implements Helper {

    private boolean debug = false;

    private List<Chunk> chunkList;
    private Predicate<Entity> filter;
    private long delayBetweenChunks = -1;

    public AsyncEntitiesGather(List<Chunk> chunkList, Predicate<Entity> filter){

        this.chunkList = chunkList;
        this.filter = filter;

    }

    public AsyncEntitiesGather debug(boolean debug){
        this.debug = debug;
        return this;
    }

    public Stack<Entity> complete() {
        Stack<Entity> entityStack = new Stack<>();
        boolean isAsync = isAsyncThread();
        try {

            if(debug) {
                print("&b<START=AsyncEntitiesGather, CHUNKS=" + chunkList.size() + ">");
            }
            long currentTime = System.currentTimeMillis();

            for(Chunk chunk : chunkList) {

                try {

                    for (Entity ent : chunk.getEntities()) {
                        if(ent == null || ent.isDead() || !filter.test(ent)) continue;
                        entityStack.push(ent);
                    }

                } catch (NoSuchElementException | ArrayIndexOutOfBoundsException ignored){}
                if(isAsync && delayBetweenChunks != -1) Thread.sleep(delayBetweenChunks);

            }
            if(debug) {
                print("&b<DONE TOOK: " + (System.currentTimeMillis() - currentTime) + "ms, GATHERED: " + entityStack.size() +">");
            }

        } catch (Exception ex){
            Energiser.getInstance().getHandler().getErrorManager().error(ex.getMessage(), ex, 3);
        }
        return entityStack;
    }

    public long delayBetweenChunks() {
        return delayBetweenChunks;
    }

    public void delayBetweenChunks(long delayBetweenChunks) {
        this.delayBetweenChunks = delayBetweenChunks;
    }
}
