package org.brian.core.async;

import org.bukkit.Material;
import org.bukkit.block.Block;

public class CachedBlock {

    private Block block;
    private Material lastType;

    public CachedBlock(Block block){

        this.block = block;
        this.lastType = block.getType();

    }

    public Material getLastType() {
        return lastType;
    }

    public Material getType(){
        return block.getType();
    }

    public boolean hasChanged(){
        return lastType != block.getType();
    }

    public boolean exists(){
        return block.getType() != Material.AIR;
    }

    public Block getBlock() {
        return block;
    }
}
