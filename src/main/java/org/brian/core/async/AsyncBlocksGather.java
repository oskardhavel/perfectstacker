package org.brian.core.async;

import org.brian.core.cuboid.Cuboid;
import org.brian.core.energiser.Energiser;
import org.brian.core.helper.Helper;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;
import java.util.function.Predicate;

public class AsyncBlocksGather implements Helper {

    private boolean debug = false;
    private List<Chunk> chunkList;
    private Predicate<Block> filter;
    private long delayBetweenChunks = -1;

    public AsyncBlocksGather(List<Chunk> chunkList, Predicate<Block> filter){

        this.chunkList = chunkList;
        this.filter = filter;

    }

    public AsyncBlocksGather debug(boolean debug){
        this.debug = debug;
        return this;
    }

    public Stack<CachedBlock> complete() {
        Stack<CachedBlock> blockStack = new Stack<>();
        boolean isAsync = isAsyncThread();
        try {
            if (debug) {
                print("&b<START=AsyncBlocksGather, CHUNKS=" + chunkList.size() + ">");
            }

            long currentTime = System.currentTimeMillis();
            List<Block> added = new ArrayList<>();

            for (Chunk chunk : chunkList) {

                Location corner1 = chunk.getBlock(0, 0, 0).getLocation();
                Location corner2 = chunk.getBlock(15, 0, 15).getLocation();

                for (int y = getStartingY(chunk); y != 1; y--) {

                    Location corner1Clone = corner1.clone();
                    Location corner2Clone = corner2.clone();

                    corner1Clone.setY(y);
                    corner2Clone.setY(y);

                    Cuboid cuboid = new Cuboid(corner1Clone, corner2Clone);
                    Block lastBlock = null;
                    Block[] blockArray = cuboid.blockArray();

                    for (int i = blockArray.length-1; i > 0; i--) {

                        Block block = blockArray[i];
                        if(block == null) continue;

                        if (filter.test(block) && !added.contains(block) && !isConnected(block, lastBlock)) {
                            lastBlock = block;
                            blockStack.push(new CachedBlock(block));
                            added.add(block);
                        }

                    }
                }
                if(isAsync && delayBetweenChunks != -1) Thread.sleep(delayBetweenChunks);
            }
            if (debug) {
                print("&b<DONE TOOK: " + (System.currentTimeMillis() - currentTime) + "ms, COLLECTED: " + blockStack.size() + ">");
            }
            return blockStack;
        } catch (Exception ex) {
            Energiser.getInstance().getHandler().getErrorManager().error(ex.getMessage(), ex, 1);
            return blockStack;
        }
    }

    private int getStartingY(Chunk chunk) {

        Location corner1 = chunk.getBlock(0, 0, 0).getLocation();
        Location corner2 = chunk.getBlock(15, 0, 15).getLocation();
        List<Integer> heights = new ArrayList<>();

        Cuboid cuboid = new Cuboid(corner1, corner2);
        World world = chunk.getWorld();
        Block[] blockArray = cuboid.blockArray();
        Block block;

        for(int i = blockArray.length-1; i > 0; i--){
            block = blockArray[i];
            if(block == null) continue;
            Block uppestBlock =  world.getHighestBlockAt(block.getLocation());
            if (!heights.contains(uppestBlock.getY())) heights.add(uppestBlock.getY());
        }

        return heights.stream().max(Comparator.comparing(Integer::valueOf)).orElse(255);

    }

    public boolean isConnected(Block block1, Block block2){

        if(block1 == null || block2 == null) return false;
        return block2.getY() - block1.getY() == 1 && block2.getZ() == block1.getZ() && block2.getX() == block1.getX();

    }

    public long delayBetweenChunks() {
        return delayBetweenChunks;
    }

    public void delayBetweenChunks(long delayBetweenChunks) {
        this.delayBetweenChunks = delayBetweenChunks;
    }

}
