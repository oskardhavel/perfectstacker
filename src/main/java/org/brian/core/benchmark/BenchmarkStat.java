package org.brian.core.benchmark;

import org.apache.commons.lang3.StringUtils;

import static org.brian.core.helper.HelperStatic.array;

public class BenchmarkStat {

    private long cpuUsage;
    private long delay;
    private long took;

    public BenchmarkStat(long cpuUsage, long delay, long took) {
        this.cpuUsage = cpuUsage;
        this.delay = delay;
        this.took = took;
    }

    public long getDelay() {
        return delay;
    }

    public long getTook() {
        return took;
    }

    public long getCpuUsage() {
        return cpuUsage;
    }

    @Override
    public String toString() {
        return "{cpu:" + cpuUsage + ", delay:" + delay + ", took:" + took + "}";
    }

    public static BenchmarkStat fromString(String string){

        String split[] = StringUtils.replaceEach(string, array("{", "}", "\\s+"), array("", "", "")).split(",");

        long cpuUsage, delay, took;

        cpuUsage = Long.parseLong(split[0].split(":")[1]);
        delay = Long.parseLong(split[1].split(":")[1]);
        took = Long.parseLong(split[2].split(":")[1]);

        return new BenchmarkStat(cpuUsage, delay, took);

    }

    public void optimize() {

        this.delay = delay / 2;

    }
}
