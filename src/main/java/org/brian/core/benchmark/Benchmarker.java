package org.brian.core.benchmark;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Benchmarker {

    private final long minDelay;
    private final long maxDelay;
    private long afterDelay;
    private Instant start;
    private Instant end;
    private long lastDelay = 0;
    private long sleepTime = 0;
    private List<BenchmarkStat> benchmarkValues = new ArrayList<>();
    private CpuWatcher watcher;

    private long startUsage;
    private long endUsage;

    public Benchmarker(int minDelay, int maxDelay) {

        this.maxDelay = maxDelay;
        this.minDelay = minDelay;
        this.afterDelay = ThreadLocalRandom.current().nextLong((maxDelay - minDelay) + 1) + minDelay;
        this.watcher = new CpuWatcher();
        this.watcher.start();
    }

    public void sleepAfter() {

        sleep(afterDelay);
        lastDelay = afterDelay;
        afterDelay = ThreadLocalRandom.current().nextLong((maxDelay - minDelay) + 1) + minDelay;

    }

    private void sleep(long ms) {
        try {
            Thread.sleep(ms);
            sleepTime = ms;
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void start(){
        this.start = Instant.now();
        long threadId = Thread.currentThread().getId();
        CpuWatcher.CpuUsage usage = watcher.getThreadCpuUsages().stream().filter(u -> u.getThreadId() == threadId).findFirst().orElse(null);
        if(usage != null) this.startUsage = usage.getLastUpdateTime();
    }

    public void stop() {
        this.end = Instant.now();
        long threadId = Thread.currentThread().getId();
        CpuWatcher.CpuUsage usage = watcher.getThreadCpuUsages().stream().filter(u -> u.getThreadId() == threadId).findFirst().orElse(null);
        if(usage != null) this.endUsage = usage.getLastUpdateTime();
    }

    public void calculate() {

        try {

            benchmarkValues.add(new BenchmarkStat((endUsage-startUsage), lastDelay, Duration.between(start, end).toMillis() - sleepTime));

        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public BenchmarkStat bestResult() {

        List<Long> cpuUsages = new ArrayList<>();
        benchmarkValues.forEach(b -> cpuUsages.add(b.getCpuUsage()));

        Collections.sort(cpuUsages);
        Collections.reverse(cpuUsages);

        Long midResult = cpuUsages.get(Math.round(cpuUsages.size()/2));
        BenchmarkStat stat = benchmarkValues.stream().filter(b -> b.getCpuUsage() == midResult).findFirst().orElse(null);
        stat.optimize();

        return stat;

    }

    public void end() {
        watcher.stop();
    }
}
