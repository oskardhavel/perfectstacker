package org.brian.core.version;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;

public class VersionController {

    private int version;
    private String stringVersion;

    public VersionController(){

        String fullVersion = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        this.stringVersion = StringUtils.replace(fullVersion, "_", " ");
        this.version = Integer.parseInt(StringUtils.replace(fullVersion.split("_")[1].split("_R")[0], "v", ""));

    }

    public String stringVersion() {
        return stringVersion;
    }

    public boolean is(int version){
        return this.version == version;
    }

    public boolean isAfter(int version){
        return this.version > version;
    }

    public boolean isBefore(int version){
        return this.version < version;
    }

    public boolean isOrAfter(int version){
        return this.version == version || this.version > version;
    }

}
