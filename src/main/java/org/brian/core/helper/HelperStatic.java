package org.brian.core.helper;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface HelperStatic {

    static <T> List<T> toObjectsList(T... elements){
        return new ArrayList<>(Arrays.asList(elements));
    }

    static <T> T[] array(T... elements){
        return elements;
    }

    static void print(Object text){
        Bukkit.getConsoleSender().sendMessage(color(text.toString()));
    }

    static String color(String text){
        return ChatColor.translateAlternateColorCodes('&', text);
    }
}
