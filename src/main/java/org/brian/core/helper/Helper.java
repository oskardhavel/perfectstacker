package org.brian.core.helper;

import org.brian.core.Core;
import org.brian.core.energiser.Energiser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public interface Helper {

    default <T> List<T> toObjectsList(T... elements){
        return new ArrayList<>(Arrays.asList(elements));
    }

    default void print(Object text){
        Bukkit.getConsoleSender().sendMessage(color(text.toString()));
    }

    default String color(String text){
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    default boolean isAsyncThread(){
        return !Thread.currentThread().getName().equalsIgnoreCase("Server thread");
    }

    default void async(Runnable runnable, boolean newThread) {
        Energiser.getInstance().runTaskAsync(runnable, newThread);
    }

    default void sync(Runnable runnable){
        Energiser.getInstance().runSyncTask(runnable, Core.getInstance().getPlugin());
    }

    default void async(Runnable runnable){
        async(runnable, false);
    }

    default <T> List<List<T>> splitList(final List<T> ls, final int iParts)
    {
        final List<List<T>> lsParts = new ArrayList<>();
        final int iChunkSize = ls.size() / iParts;
        int iLeftOver = ls.size() % iParts;
        int iTake = iChunkSize;

        for( int i = 0, iT = ls.size(); i < iT; i += iTake )
        {
            if( iLeftOver > 0 )
            {
                iLeftOver--;

                iTake = iChunkSize + 1;
            }
            else
            {
                iTake = iChunkSize;
            }

            lsParts.add( new ArrayList<>( ls.subList( i, Math.min( iT, i + iTake ) ) ) );
        }

        return lsParts;
    }

    default boolean collectionContains(Object object, Collection collection){
        return collection.contains(object);
    }

    default <T> boolean arrayContains(T object, T[] array){
        return Arrays.stream(array).
                anyMatch(target -> target.equals(object));
    }

}
