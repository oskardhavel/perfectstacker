package org.brian.core;

import org.brian.core.command.CommandController;
import org.brian.core.helper.Helper;
import org.brian.core.mi.MenuListener;
import org.brian.core.particles.OOPParticle;
import org.brian.core.version.VersionController;
import org.bukkit.plugin.java.JavaPlugin;

public class Core implements Helper {

    private JavaPlugin plugin;
    private VersionController versionController;
    private CommandController commandManager;
    private OOPParticle particleProvider;
    private static Core instance;

    public Core(JavaPlugin plugin){

        instance = this;
        this.plugin = plugin;
        this.commandManager = new CommandController(plugin);
        this.versionController = new VersionController();
        this.particleProvider = OOPParticle.newInstance();
        new MenuListener(plugin);

    }

    public CommandController getCommandManager() {
        return commandManager;
    }
    public VersionController getVersionController() {
        return versionController;
    }
    public static Core getInstance() {
        return instance;
    }
    public OOPParticle getParticleProvider() {
        return particleProvider;
    }
    public JavaPlugin getPlugin() {
        return plugin;
    }

}
