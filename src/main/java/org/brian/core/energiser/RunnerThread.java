package org.brian.core.energiser;

import java.util.ArrayList;
import java.util.List;

public class RunnerThread extends Thread {

    private boolean shutdown = false;
    private List<ETask> tasks = new ArrayList<>();

    public RunnerThread(String name) {
        super(name);
        start();
    }

    public void assignTask(ETask task) {
        if(!tasks.contains(task)) {
            tasks.add(task);
        }
    }

    @Override
    public void run() {
        while (!shutdown) {

            try {
                if (tasks.isEmpty()) sleep(1);
                List<ETask> tasksCopy = new ArrayList<>(tasks);
                for (ETask task : tasksCopy) {
                    try {
                        task.taskConsumer().accept(task);
                    } catch (Exception ex) {
                        Energiser.getInstance().getHandler().getErrorManager().error(ex.getMessage(), ex, 3);
                    }
                    tasks.remove(task);
                }

            } catch (Exception ex){
                Energiser.getInstance().getHandler().getErrorManager().error(ex.getMessage(), ex, 3);
            }

        }

        interrupt();

    }

    public boolean isShutdown() {
        return shutdown;
    }

    public void shutdown() {
        this.shutdown = true;
    }
}
