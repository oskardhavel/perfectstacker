package org.brian.core.energiser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.stream.Collectors.toList;

public class EnergiserScheduler extends Thread {

    private final List<ETask> tasks = new ArrayList<>();
    private final Map<ETask, AtomicLong> delays = new HashMap<>();
    private final RunnerThread runnerThread;

    private boolean shutdown = false;

    public EnergiserScheduler(String name) {
        super("Energiser-" + name + "-Scheduler");
        this.runnerThread = new RunnerThread("Energiser-" + name + "-Runner");
        start();
    }

    public void schedule(ETask task){
        if(task.delay() == -1) {
            runnerThread.assignTask(task);
        } else tasks.add(task);
    }

    public void schedule(Runnable runnable){
        runnerThread.assignTask(new ETask((t) -> runnable.run()));
    }

    public ETask scheduleDelayed(Runnable runnable, long time){
        return scheduleDelayed(runnable, time, TimeUnit.MILLISECONDS);
    }

    public ETask scheduleDelayed(Runnable runnable, long time, TimeUnit unit){
        ETask task = new ETask((t) -> runnable.run()).delay(unit.toMillis(time));
        tasks.add(task);
        return task;
    }

    public ETask scheduleRepeated(Runnable runnable, long time){
        return scheduleRepeated(runnable, time, TimeUnit.MILLISECONDS);
    }

    public ETask scheduleRepeated(Runnable runnable, long time, TimeUnit unit){
        ETask task = new ETask((t) -> runnable.run()).delay(unit.toMillis(time)).repeat(true);
        tasks.add(task);
        return task;
    }

    @Override
    public void run() {

        try {
            while (!shutdown) {

                if (tasks.isEmpty()) {
                    TimeUnit.MILLISECONDS.sleep(1);
                    continue;
                }

                List<ETask> tasksClone = new ArrayList<>(tasks);

                for(ETask task : tasksClone.stream().filter(task -> task.priority() == ETask.TaskPriority.FIRST).collect(toList())){

                    if(task.isShutdown()){
                        tasks.remove(task);
                        continue;
                    }
                    runTask(task);
                }
                for(ETask task : tasksClone.stream().filter(task -> task.priority() == ETask.TaskPriority.MIDDLE).collect(toList())){
                    if(task.isShutdown()){
                        tasks.remove(task);
                        continue;
                    }
                    runTask(task);
                }
                for(ETask task : tasksClone.stream().filter(task -> task.priority() == ETask.TaskPriority.LAST).collect(toList())){
                    if(task.isShutdown()){
                        tasks.remove(task);
                        continue;
                    }
                    runTask(task);
                }
                TimeUnit.MILLISECONDS.sleep(1);

            }
            interrupt();
        } catch (Exception ex){
            Energiser.getInstance().getHandler().getErrorManager().error(ex.getMessage(), ex, 3);
        }

    }

    public boolean isShutdown() {
        return shutdown;
    }

    public void shutdown() {
        runnerThread.shutdown();
        this.shutdown = true;
    }

    private void runTask(ETask task){

        AtomicLong delay = delays.containsKey(task) ? delays.get(task) : new AtomicLong(task.delay());
        if(!delays.containsKey(task)) delays.put(task, delay);
        delay.decrementAndGet();

        if(delay.get() == 0) {

            runnerThread.assignTask(task);

            if(task.repeat()) delay.set(task.delay());
            else {
                delays.remove(task);
                tasks.remove(task);
            }

        }
    }

    public int tasksCount(){
        return tasks.size();
    }

}
