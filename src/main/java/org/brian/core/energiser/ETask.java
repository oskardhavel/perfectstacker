package org.brian.core.energiser;

import java.util.function.Consumer;

public class ETask {

    private Consumer<ETask> taskConsumer;

    private long delay = -1;
    private boolean repeat = false;
    private TaskPriority priority = TaskPriority.MIDDLE;
    private boolean shutdown;

    public ETask(Consumer<ETask> taskConsumer){
        this.taskConsumer = taskConsumer;
    }
    public Consumer<ETask> taskConsumer() {
        return taskConsumer;
    }
    public long delay() {
        return delay;
    }

    public ETask delay(long delay){
        this.delay = delay;
        return this;
    }

    public boolean repeat() {
        return repeat;
    }

    public ETask repeat(boolean bool){
        this.repeat = bool;
        return this;
    }

    public TaskPriority priority() {
        return priority;
    }

    public ETask priority(TaskPriority priority) {
        this.priority = priority;
        return this;
    }

    public enum TaskPriority {

        FIRST,
        MIDDLE,
        LAST

    }

    public ETask shutdown(){
        this.shutdown = true;
        return this;
    }

    public boolean isShutdown() {
        return shutdown;
    }
}
