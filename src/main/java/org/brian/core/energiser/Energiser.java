package org.brian.core.energiser;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;

public class Energiser {

    private static Energiser instance;
    private EnergiserScheduler scheduler;
    private ExecutorService asyncPool;
    final private Handler handler;

    public Energiser(String name, Handler handler2) {

        instance = this;
        this.asyncPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()/2, r -> new Thread(r, "Energiser-" + name + "-Async Thread"));
        this.scheduler = new EnergiserScheduler(name);
        handler = handler2;

    }

    public ETask runTaskAsync(Runnable runnable, boolean asyncScheduler) {

        ETask task = new ETask((t) -> runnable.run());

        if(!asyncScheduler){

            scheduler.schedule(task);

        } else {
            if (asyncPool.isShutdown() || asyncPool.isTerminated()) return null;

            try {
                asyncPool.submit(() -> task.taskConsumer().accept(task));
            } catch (Exception ex) {
                getHandler().getErrorManager().error(ex.getMessage(), ex, 1);
            }
        }

        return task;

    }

    public ETask runTaskDelayed(Runnable runnable, int delay, TimeUnit unit){

        if(scheduler.isShutdown()) return null;

        return scheduler.scheduleDelayed(runnable, delay, unit);

    }

    public ETask runRepeatingTask(Runnable runnable, int timer, TimeUnit unit){

        if(scheduler.isShutdown()) return null;
        return scheduler.scheduleRepeated(runnable, timer, unit);

    }

    public void runSyncTask(Runnable runnable, JavaPlugin plugin){

        Bukkit.getScheduler().runTask(plugin, runnable);

    }

    public void shutdown(){

        instance = null;

        try {
            asyncPool.shutdown();
            asyncPool.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            getHandler().getErrorManager().error(e.getMessage(), e, 3);
        }
        finally {
            asyncPool.shutdownNow();
        }

        scheduler.shutdown();

    }

    public Handler getHandler() {
        return handler;
    }

    public static Energiser getInstance() {
        return instance;
    }

}
