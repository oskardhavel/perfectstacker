package org.brian.core.utils;

import org.brian.core.Core;
import org.brian.core.energiser.Energiser;
import org.brian.core.helper.Helper;
import org.brian.core.nbt.NBTEntity;
import org.brian.perfectstacker.utils.HChunk;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class AsyncUtils implements Helper {

    private static JavaPlugin plugin;

    public AsyncUtils(JavaPlugin plugin2) {
        plugin = plugin2;
    }

    public static void setBlock(Location location, Material type) {
        Energiser.getInstance().runSyncTask(() -> location.getBlock().setType(type), plugin);
    }

    public static List<LivingEntity> getLivingEntities(Location where, Double radius, CoreEnums.RangeType radiusType, List<EntityType> types, CoreEnums.EntitiesGatherMethod gatherMethod, Predicate<LivingEntity> filter) {

                    CompletableFuture<List<LivingEntity>> entities = new CompletableFuture<>();

                    if (radiusType == CoreEnums.RangeType.BLOCKS) {

                        double chunkRadius = radius > 16 ? Math.round(radius / 16) * 16 : radius;
                        if(chunkRadius < 1) chunkRadius = 1;

                        List<LivingEntity> ents = getLivingEntities(where, chunkRadius, CoreEnums.RangeType.CHUNKS, types, gatherMethod, filter);
                        ents.removeIf((entity) -> entity.getLocation().distance(where) > radius);
                        return ents;

                    } else {
                        Energiser.getInstance().runSyncTask(() -> {

                            int chunkRadius = (int) Math.round(radius);
                            List<Chunk> chunksToGoThrough = new ArrayList<>();
                            if(chunkRadius > 1) chunksToGoThrough.addAll(getChunksAround(where.getChunk(), (int) Math.round(radius), true));
                            else chunksToGoThrough.add(where.getChunk());

                            List<LivingEntity> ents = new ArrayList<>();

                            for (Chunk chunk : chunksToGoThrough) {
                                List<Entity> entityList = new ArrayList<>(Arrays.asList(chunk.getEntities()));
                                ents.addAll(entityList.stream().filter(ent -> ent instanceof LivingEntity).map(ent -> (LivingEntity) ent).collect(toList()));
                            }

                            if (gatherMethod == CoreEnums.EntitiesGatherMethod.BLACKLIST) {
                    ents = ents.stream().filter(entity -> !types.contains(entity.getType())).collect(Collectors.toList());
                } else if(gatherMethod == CoreEnums.EntitiesGatherMethod.WHITELIST){
                    ents = ents.stream().filter(entity -> types.contains(entity.getType())).collect(Collectors.toList());
                }

                if (filter != null) ents = ents.stream().filter(filter).collect(Collectors.toList());

                entities.complete(ents);

            }, plugin);
        }
        try {
            return entities.get();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static List<LivingEntity> getLivingEntities(Location where, Double radius, CoreEnums.RangeType radiusType) {
        return getLivingEntities(where, radius, radiusType, new ArrayList<>(), CoreEnums.EntitiesGatherMethod.NONE, null);
    }

    public static List<LivingEntity> getLivingEntities(Location where, Double radius, CoreEnums.RangeType radiusType, Predicate<LivingEntity> filter) {
        return getLivingEntities(where, radius, radiusType, new ArrayList<>(), CoreEnums.EntitiesGatherMethod.NONE, filter);
    }

    public static boolean isChunkLoaded(int chunkX, int chunkZ, World world) {

        boolean toReturn = world.isChunkLoaded(chunkX, chunkZ);

        if (toReturn) {
            if (containsPlayersAroundChunk(world.getChunkAt(chunkX, chunkZ))) {
                return true;
            }
        }
        return false;
    }

    public static List<Chunk> getChunksAround(int chunkX, int chunkZ, int radius, World world, boolean checkForLoaded) {

        if(Thread.currentThread().getName().equalsIgnoreCase("Server thread")){
            List<Chunk> chunks = new ArrayList<>();
            for (int x = chunkX - radius; x <= chunkX + radius; x++) {
                for (int z = chunkZ - radius; z <= chunkZ + radius; z++) {

                    if (checkForLoaded && !isChunkLoaded(x, z, world)) {
                        continue;
                    }
                    chunks.add(world.getChunkAt(x, z));
                }
            }
            return chunks;
        } else {

            CompletableFuture<List<Chunk>> future = new CompletableFuture<>();
            Energiser.getInstance().runSyncTask(() -> {

                List<Chunk> chunks = new ArrayList<>();
                for (int x = chunkX - radius; x <= chunkX + radius; x++) {
                    for (int z = chunkZ - radius; z <= chunkZ + radius; z++) {

                        if (checkForLoaded && !isChunkLoaded(x, z, world)) {
                            continue;
                        }
                        chunks.add(world.getChunkAt(x, z));
                    }
                }
                future.complete(chunks);

            }, Core.getInstance().getPlugin());

            try{
                return future.get();
            } catch (Exception ex){
                ex.printStackTrace();
            }

        }

        return new ArrayList<>();

    }

    public static List<Chunk> getChunksAround(Chunk chunk, int radius, boolean checkForLoaded) {
        return getChunksAround(chunk.getX(), chunk.getZ(), radius, chunk.getWorld(), checkForLoaded);
    }

    public static boolean containsPlayersAroundLocation(Location location) {

        List<Chunk> loadedChunks = new ArrayList<>();
        int serverViewDistance = Bukkit.getServer().getViewDistance();

        int chunkX = location.getBlockX() >> 4;
        int chunkZ = location.getBlockZ() >> 4;

        World world = location.getWorld();
        Chunk mainChunk = world.getChunkAt(chunkX, chunkZ);
        if (mainChunk == null) return false;

        try {
            List<Entity> entities = Collections.unmodifiableList(Arrays.asList(mainChunk.getEntities()));
            if (entities.stream().filter(entity -> entity != null && entity.getType() == EntityType.PLAYER).collect(toList()).size() > 0)
                return true;
        } catch (NoSuchElementException ignored) {
        }
        for (int x = chunkX - serverViewDistance; x <= chunkX + serverViewDistance; x++) {
            for (int z = chunkZ - serverViewDistance; z <= chunkZ + serverViewDistance; z++) {
                if (world.isChunkLoaded(x, z)) loadedChunks.add(world.getChunkAt(x, z));
            }
        }

        int found = 0;
        for (Chunk chunk : loadedChunks) {

            try {

                if (chunk == null) continue;
                List<Entity> entities2 = Collections.unmodifiableList(Arrays.asList(chunk.getEntities())).stream().filter(entity -> entity != null && entity.getType() == EntityType.PLAYER).collect(Collectors.toList());
                if (entities2.isEmpty()) continue;

                found = found + entities2.size();
            } catch (NoSuchElementException ignored) {
            }

        }
        return found > 0;
    }

    public static boolean containsPlayersAroundChunk(Chunk chunk) {

        List<Chunk> loadedChunks = new ArrayList<>();
        int serverViewDistance = Bukkit.getServer().getViewDistance();

        int chunkX = chunk.getX();
        int chunkZ = chunk.getZ();

        World world = chunk.getWorld();
        Chunk mainChunk = world.getChunkAt(chunkX, chunkZ);
        if (mainChunk == null) return false;

        try {
            List<Entity> entities = new ArrayList<>();
            for (Entity ent : mainChunk.getEntities()) {
                entities.add(ent);
            }
            if (entities.stream().filter(entity -> entity != null && entity.getType() == EntityType.PLAYER).collect(toList()).size() > 0)
                return true;
        } catch (NoSuchElementException | ArrayIndexOutOfBoundsException ignored) {
        }
        for (int x = chunkX - serverViewDistance; x <= chunkX + serverViewDistance; x++) {
            for (int z = chunkZ - serverViewDistance; z <= chunkZ + serverViewDistance; z++) {
                if (world.isChunkLoaded(x, z)) loadedChunks.add(world.getChunkAt(x, z));
            }
        }

        int found = 0;
        for (Chunk chunk2 : loadedChunks) {

            try {

                if (chunk2 == null) continue;
                List<Entity> entities2 = new ArrayList<>();
                for (Entity ent : chunk2.getEntities()) {
                    entities2.add(ent);
                }
                entities2 = entities2.stream().filter(entity -> entity != null && entity.getType() == EntityType.PLAYER).collect(Collectors.toList());
                if (entities2.isEmpty()) continue;

                found = found + entities2.size();
            } catch (NoSuchElementException | ArrayIndexOutOfBoundsException ignored) {
            }

        }
        return found > 0;
    }

    public static boolean removeItem(ItemStack item, int amt, Inventory inv) {

        if (!inv.containsAtLeast(item, amt)) {
            return false;
        }

        ItemStack currentItem;
        for (int i = 0; i < 36; i++) {
            if ((currentItem = inv.getItem(i)) != null && currentItem.isSimilar(item)) {
                if (currentItem.getAmount() >= amt) {

                    if ((currentItem.getAmount() - amt) <= 0) inv.setItem(i, new ItemStack(Material.AIR));
                    else currentItem.setAmount(currentItem.getAmount() - amt);

                    return true;

                } else {
                    amt -= currentItem.getAmount();
                    inv.setItem(i, null);
                }
            }
        }
        return false;
    }

    public static void teleportAndDisable(List<LivingEntity> entities, Location hopperMiddle) {

        Energiser.getInstance().runSyncTask(() -> {

            entities.forEach(entity -> {

                NBTEntity nbtEntity = new NBTEntity(entity);
                nbtEntity.setByte("NoAI", (byte) 1);
                entity.teleport(hopperMiddle);

            });

        }, plugin);

    }

    public static void teleportAndDisable(LivingEntity entity, Location hopperMiddle) {

        Energiser.getInstance().runSyncTask(() -> {

            NBTEntity nbtEntity = new NBTEntity(entity);
            nbtEntity.setByte("NoAI", (byte) 1);
            entity.teleport(hopperMiddle);

        }, plugin);

    }

    public static void teleportAndDisableAndDamage(LivingEntity entity, Location hopperMiddle, double damageAmount) {

        Energiser.getInstance().runSyncTask(() -> {

            NBTEntity nbtEntity = new NBTEntity(entity);
            nbtEntity.setByte("NoAI", (byte) 1);
            entity.damage(damageAmount);
            entity.teleport(hopperMiddle);

        }, plugin);

    }

    public static void damage(LivingEntity entity, int amount) {

        Energiser.getInstance().runSyncTask(() -> {

            entity.damage(amount);

        }, plugin);

    }

    public static List<Entity> spawnEntity(Location location, EntityType type, int amount){

        CompletableFuture<List<Entity>> future = new CompletableFuture<>();

        Energiser.getInstance().runSyncTask(() -> {

            List<Entity> entityList = new ArrayList<>();

            for(int i = amount; i != 0; i--) {
                entityList.add(location.getWorld().spawnEntity(location, type));
            }

            future.complete(entityList);

        }, plugin);

        try {
            return future.get();
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return new ArrayList<>();
    }

}
