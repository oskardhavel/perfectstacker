package org.brian.core.utils;

import org.apache.commons.math3.util.Pair;
import org.brian.core.helper.HelperStatic;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static org.brian.core.helper.HelperStatic.print;
import static org.brian.core.helper.HelperStatic.toObjectsList;

public class InventoryUtil implements HelperStatic {

    public static int countItemsFilteredByItem(List<Inventory> inventories, Predicate<ItemStack> filter){

        AtomicInteger count = new AtomicInteger();
        for(Inventory inventory : inventories){

            IntStream.range(0, inventory.getSize()).forEach(slot -> {

                ItemStack item = inventory.getItem(slot);
                if(item == null || item.getType() == Material.AIR) return;
                if(filter != null && !filter.test(item)) return;

                count.addAndGet(item.getAmount());

            });

        }
        return count.get();
    }

    public static int countItemsFilteredBySlot(List<Inventory> inventories, Predicate<Integer> filter){

        AtomicInteger count = new AtomicInteger();
        for(Inventory inventory : inventories){

            IntStream.range(0, inventory.getSize()).forEach(slot -> {

                if(filter != null && !filter.test(slot)) return;

                ItemStack item = inventory.getItem(slot);
                if(item == null || item.getType() == Material.AIR) return;

                count.addAndGet(item.getAmount());

            });

        }
        return count.get();
    }

    public static int countItems(List<Inventory> inventories){
        return countItemsFilteredByItem(inventories, null);
    }

    public static int countItems(Inventory inventory){
        return countItemsFilteredByItem(toObjectsList(inventory), null);
    }

    public static void throwInfo(Inventory inventory){

        print("");
        print("&6THROWING INVENTORY...");
        print("&eSIZE: " + inventory.getSize());
        print("&eTITLE: " + inventory.getTitle());
        print("");
        print("&6==== ITEMS ====");
        IntStream.range(0, inventory.getSize()).forEach(slot -> {

            ItemStack item = inventory.getItem(slot);
            if(item == null) return;

            print("&e- " + slot + ", " + item);

        });
        print("");

    }

    public static Map<Integer, ItemStack> getItemsFilteredByItem(Inventory inventory, Predicate<ItemStack> filter){

        Map<Integer, ItemStack> items = new HashMap<>();
        if (inventory != null) {

            IntStream.range(0, inventory.getSize()).forEach(slot -> {

                ItemStack item = inventory.getItem(slot);
                if(item == null || item.getType() == Material.AIR) return;
                if(filter != null && !filter.test(item)) return;

                items.put(slot, item);

            });

        }

        return items;

    }

    public static Map<Integer, ItemStack> getItemsFilteredBySlot(Inventory inventory, Predicate<Integer> filter){

        Map<Integer, ItemStack> items = new HashMap<>();
        if (inventory != null) {

            IntStream.range(0, inventory.getSize()).forEach(slot -> {

                if(filter != null && !filter.test(slot)) return;

                ItemStack item = inventory.getItem(slot);
                if(item == null || item.getType() == Material.AIR) return;

                items.put(slot, item);

            });

        }

        return items;

    }

    public static Pair<ItemStack, Integer> findFirstAvailableItem(Inventory inventory, Predicate<ItemStack> filter){

        for(int slot = 0; slot < inventory.getSize(); slot++){

            ItemStack itemStack = inventory.getItem(slot);
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;
            if(filter != null && !filter.test(itemStack)) continue;

            return new Pair<>(itemStack, slot);

        }

        return null;
    }

    public static Collection<ItemStack> getItems(Inventory inventory){
        return getItemsFilteredByItem(inventory, null).values();
    }

    public static Pair<ItemStack, Integer> findFirstAvailableItem(Inventory inventory){
        return findFirstAvailableItem(inventory, null);
    }

    public static int removeItem(Inventory inventory, ItemStack item){
        return removeItem(toObjectsList(inventory), item);
    }

    public static int removeItem(List<Inventory> inventories, ItemStack item){

        AtomicInteger removed = new AtomicInteger(0);
        if(item == null) return removed.get();

        for(Inventory inventory : inventories){

            for(ItemStack item2 : getItems(inventory)){

                if(item.getAmount() <= 0) return removed.get();

                if(item2.getAmount() >= item.getAmount()){

                    if(item2.getAmount() - item.getAmount() == 0) item2.setAmount(0);
                    else item2.setAmount(item2.getAmount() - item.getAmount());

                    removed.addAndGet(item.getAmount());
                    item.setAmount(0);

                } else {

                    int canBeRemoved = item.getAmount() - item2.getAmount();
                    item2.setAmount(item2.getAmount() - canBeRemoved);
                    item.setAmount(item.getAmount() - canBeRemoved);
                    removed.addAndGet(canBeRemoved);

                }

            }

        }
        return removed.get();

    }


    public static int addItem(Inventory inventory, ItemStack item){
        return addItem(toObjectsList(inventory), toObjectsList(item));
    }

    public static int addItem(Inventory inventory, List<ItemStack> items){
        return addItem(toObjectsList(inventory), items);
    }

    public static int addItem(List<Inventory> inventories, List<ItemStack> items){

        AtomicInteger added = new AtomicInteger(0);
        if(items == null || items.isEmpty()) return added.get();

        for(ItemStack item : items) {
            int startingAmount = item.getAmount();
            for (Inventory inventory : inventories) {

                if (added.get() == startingAmount) return added.get();
                for (ItemStack item2 : getItemsFilteredByItem(inventory, (filteredItem) -> filteredItem.isSimilar(item) && filteredItem.getAmount() < 64).values()) {

                    if (added.get() == startingAmount) return added.get();
                    int canFit = item2.getMaxStackSize() - item2.getAmount();
                    if (canFit >= item.getAmount()) {

                        item2.setAmount(item2.getAmount() + item.getAmount());
                        added.addAndGet(item.getAmount());
                        item.setAmount(0);

                    } else {

                        int howMuchCanActuallyFit = item.getAmount() - canFit;
                        if (howMuchCanActuallyFit <= canFit) {

                            item2.setAmount(item2.getAmount() + howMuchCanActuallyFit);
                            item.setAmount(howMuchCanActuallyFit);
                            added.addAndGet(howMuchCanActuallyFit);

                        } else {

                            item2.setAmount(item2.getAmount() + canFit);
                            item.setAmount(item.getAmount() - canFit);
                            added.addAndGet(canFit);

                        }
                    }

                }

                if (added.get() != startingAmount && !getEmptySlots(inventory).isEmpty()) {
                    inventory.addItem(item);
                    added.addAndGet(item.getAmount());
                    item.setAmount(0);
                }

            }
        }

        return added.get();

    }

    public static List<Integer> getEmptySlots(Inventory inventory) {

        List<Integer> freeSlots = new ArrayList<>();

        IntStream.range(0, inventory.getSize()).forEach(slot -> {

            ItemStack item = inventory.getItem(slot);
            if(item == null || item.getType() == Material.AIR) freeSlots.add(slot);

        });

        return freeSlots;

    }

    public static void setItem(Inventory inventory, int slot, ItemStack item){
        inventory.setItem(slot, item);
    }

}