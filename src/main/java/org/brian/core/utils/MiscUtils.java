package org.brian.core.utils;

public class MiscUtils {

    public static boolean isInteger(Object object){

        if(object == null) return false;

        if(object instanceof Integer){
            return true;
        } else {

            try{
                Integer.parseInt(object.toString());
                return true;
            } catch (Exception ex){
                return false;
            }

        }

    }

}
