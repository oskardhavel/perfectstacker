package org.brian.core.utils;

public class CoreEnums {
    public enum EntitiesGatherMethod {
        BLACKLIST,
        WHITELIST,
        NONE
    }

    public enum RangeType {
        CHUNKS,
        BLOCKS
    }
}
