package org.brian.perfectstacker.stacker;

import org.brian.core.helper.HelperStatic;
import org.brian.perfectstacker.controllers.StorageController;
import org.brian.perfectstacker.entity.AStacked;
import org.brian.perfectstacker.stacker.stackers.DefaultStacker;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;

import java.util.HashMap;
import java.util.Map;

public class Stackers implements HelperStatic {

    private static Map<Class<? extends Entity>, AStacker> stackerMap = new HashMap<>();

    private static AStacker defaultEntityStacker;
    private static AStacker defaultItemStacker;

    private static boolean init = false;

    static {

        if(!init){

            init = true;
            new DefaultStacker();
            defaultEntityStacker = DefaultStacker.entityStacker();
            defaultItemStacker = DefaultStacker.itemStacker();

        }

    }

    public static void register(AStacker stacker, boolean override){

        if(stackerMap.containsKey(stacker.entity()) && override) stackerMap.remove(stacker.entity());
        stackerMap.put(stacker.entity(), stacker);

    }

    public static void register(AStacker stacker){
        register(stacker, false);
    }

    public static AStacked stack(Entity entity, Map<String, Object> data, int stackSize){

        //Safe Check
        if(StorageController.getInstance().stackedCopy().contains(entity)) StorageController.getInstance().get(entity);

        if(stackerMap.containsKey(entity.getClass())) return stackerMap.get(entity.getClass()).stack(entity, data, stackSize);
        else {

            if(entity instanceof Item) return defaultItemStacker.stack(entity, data, stackSize);
            else return defaultEntityStacker.stack(entity, data, stackSize);

        }

    }

    public static AStacked stack(Entity entity, Map<String, Object> data){
        return stack(entity, data, -1);
    }

}
