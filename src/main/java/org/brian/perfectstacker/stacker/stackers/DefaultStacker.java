package org.brian.perfectstacker.stacker.stackers;

import org.brian.core.nbt.NBTItem;
import org.brian.perfectstacker.PerfectStacker;
import org.brian.perfectstacker.configuration.PSConfig;
import org.brian.perfectstacker.controllers.ChunkController;
import org.brian.perfectstacker.controllers.EntityController;
import org.brian.perfectstacker.controllers.StorageController;
import org.brian.perfectstacker.entity.AStacked;
import org.brian.perfectstacker.entity.custom.StackedEntity;
import org.brian.perfectstacker.entity.custom.StackedItem;
import org.brian.perfectstacker.material.PSMaterial;
import org.brian.perfectstacker.stacker.AStacker;
import org.brian.perfectstacker.utils.HChunk;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class DefaultStacker {

    private static ItemStacker itemStacker;
    private static EntityStacker entityStacker;
    private static boolean init = false;

    public DefaultStacker(){

        if(!init){

            itemStacker = new ItemStacker();
            entityStacker = new EntityStacker();

        }

    }

    public static ItemStacker itemStacker() {
        return itemStacker;
    }

    public static EntityStacker entityStacker() {
        return entityStacker;
    }

    public class ItemStacker extends AStacker {

        private ItemStacker() {
            super(Item.class);
        }

        @Override
        public AStacked stack(Entity entity, Map<String, Object> data, int stackSize) {

            PSConfig.ItemsSection config = PerfectStacker.getInstance().config().itemsSection();
            if(!config.isEnabled()) return null;
            
            Location location = (Location) data.get("location");
            HChunk hChunk = ChunkController.getInstance().chunk(((Chunk) data.get("chunk")));
            Item bukkitItem = (Item)entity;
            ItemStack bukkitItemStack = bukkitItem.getItemStack().clone();

            //Settings from config
            if (config.blacklist().stream().anyMatch(pm -> {

                if(pm.data() == -1 && pm.material() == bukkitItemStack.getType()) return true;
                if(pm.item().isSimilar(bukkitItemStack)) return true;

                return false;

            })) return null;

            double radiusLimit = config.defaultMergeRadius();
            int sizeLimit = config.defaultLimit();

            if(in(bukkitItemStack, config.limit().keySet())) sizeLimit = config.limit().get(get(bukkitItemStack, config.limit().keySet()));
            if(in(bukkitItemStack, config.mergeRadius().keySet())) radiusLimit = config.mergeRadius().get(get(bukkitItemStack, config.mergeRadius().keySet()));
            if(stackSize == -1) stackSize = bukkitItemStack.getAmount();

            //Find potential friends
            int finalSizeLimit = sizeLimit;
            double finalRadiusLimit = radiusLimit;
            List<AStacked> potentialFriends = StorageController.getInstance().byFilter(stacked -> {

                if(!(stacked instanceof StackedItem)) return false;
                if (bukkitItemStack.getType() != ((StackedItem) stacked).bukkitItem().getItemStack().getType()) return false;
                if (stacked.location().getWorld() != location.getWorld()) return false;
                if (!stacked.watchedChunks().contains(hChunk)) return false;
                if(stacked.location().distance(location) > finalRadiusLimit) return false;

                return stacked.stackAmount() != finalSizeLimit;

            });

            if(potentialFriends.isEmpty()){

                NBTItem nbtItem = new NBTItem(((Item) entity).getItemStack());

                //Mark entity
                nbtItem.setInteger("PS_ITEM", stackSize);
                ((Item) entity).setItemStack(nbtItem.getItem());
                StackedItem stackedItem = new StackedItem((Item)entity, stackSize);

                return stackedItem;

            } else {

                int stackSizeCopy = stackSize;
                for (AStacked stackedFriend : potentialFriends) {

                    if (stackSizeCopy <= 0) break;
                    int leftTillMax = sizeLimit - stackedFriend.stackAmount();

                    if (leftTillMax >= stackSizeCopy) {

                        stackedFriend.increaseAmount(stackSizeCopy);
                        stackSizeCopy = 0;
                        break;

                    }

                    stackedFriend.increaseAmount(leftTillMax);
                    stackSizeCopy -= leftTillMax;
                }

                if(stackSizeCopy == 0){
                    entity.remove();
                    return null;
                }

                if(stackSizeCopy > 0) {

                    ArrayList<Integer> stackedAmountEntities = new ArrayList<>();
                    int stackAmountCopy = stackSizeCopy;

                    while (stackAmountCopy > 0) {

                        if (stackAmountCopy > sizeLimit) {

                            stackAmountCopy -= sizeLimit;
                            stackedAmountEntities.add(sizeLimit);
                            continue;

                        }
                        stackedAmountEntities.add(stackAmountCopy);
                        break;
                    }

                    List<StackedItem> spawnedItems = new ArrayList<>();
                    sync(() -> {
                        boolean entityUsed = false;
                        for(int stack : stackedAmountEntities) {

                            if(!entityUsed) {
                                
                                entityUsed = true;

                                ItemStack itemStack = bukkitItemStack.clone();
                                itemStack.setAmount(1);

                                //Mark item
                                NBTItem nbtItem = new NBTItem(itemStack);
                                nbtItem.setInteger("PS_ITEM", stack);
                                
                                ((Item) entity).setItemStack(nbtItem.getItem());
                                StackedItem stackedItem = new StackedItem(((Item) entity), stack);
                                spawnedItems.add(stackedItem);
                                continue;
                                
                            }
                            
                            ItemStack itemStack = bukkitItemStack.clone();
                            itemStack.setAmount(1);

                            //Mark item
                            NBTItem nbtItem = new NBTItem(itemStack);
                            nbtItem.setInteger("PS_ITEM", stack);

                            Item item = location.getWorld().dropItem(entity.getLocation(),  nbtItem.getItem());
                            EntityController.getInstance().ignore(item);

                            StackedItem stackedItem = new StackedItem(item, stack);
                            spawnedItems.add(stackedItem);
                            EntityController.getInstance().unIgnore(item);

                        }
                    });

                    return spawnedItems.stream().findFirst().orElse(null);

                }

            }

            return null;

        }

        private boolean in(ItemStack item, Collection<PSMaterial> materialList){

            return materialList.stream().anyMatch(pm ->{

                if(pm.data() == -1 && pm.material() == item.getType()) return true;
                if(pm.item().isSimilar(item)) return true;

                return false;

            });

        }

        private PSMaterial get(ItemStack item, Collection<PSMaterial> materialList){

            return materialList.stream().filter((pm ->{

                if(pm.data() == -1 && pm.material() == item.getType()) return true;
                if(pm.item().isSimilar(item)) return true;

                return false;

            })).findFirst().orElse(null);

        }

    }

    public class EntityStacker extends AStacker {

        private EntityStacker() {
            super(Item.class);
        }

        @Override
        public AStacked stack(Entity entity, Map<String, Object> data, int stackSize) {

            Location location = (Location) data.get("location");
            HChunk hChunk = ChunkController.getInstance().chunk(((Chunk) data.get("chunk")));

            PSConfig.EntitiesSection config = PerfectStacker.getInstance().config().entitiesSection();
            if(!config.isEnabled()) return null;

            //Settings from config
            if (config.blacklist().stream().anyMatch(pm -> config.blacklist().contains(entity.getType()))) return null;

            double radiusLimit = config.defaultMergeRadius();
            int sizeLimit = config.defaultStackLimit();

            if(config.stackLimit().containsKey(entity.getType())) sizeLimit = config.stackLimit().get(entity.getType());

            if(stackSize == -1) stackSize = 1;

            //Find potential friends
            int finalSizeLimit = sizeLimit;
            double finalRadiusLimit = radiusLimit;
            List<AStacked> potentialFriends = StorageController.getInstance().byFilter(stacked -> {

                if(!(stacked instanceof StackedEntity)) return false;
                if (entity.getType() != stacked.bukkitEntity().getType()) return false;
                if (stacked.location().getWorld() != location.getWorld()) return false;
                if (!stacked.watchedChunks().contains(hChunk)) return false;
                if(stacked.location().distance(location) > finalRadiusLimit) return false;

                return stacked.stackAmount() != 100;

            });

            if(potentialFriends.isEmpty()){

                StackedEntity stackedEntity = new StackedEntity(entity, stackSize);
                return stackedEntity;

            } else {

                int stackSizeCopy = stackSize;
                for (AStacked stackedFriend : potentialFriends) {

                    if (stackSizeCopy <= 0) break;
                    int leftTillMax = finalSizeLimit - stackedFriend.stackAmount();

                    if (leftTillMax >= stackSizeCopy) {

                        stackedFriend.increaseAmount(stackSizeCopy);
                        stackSizeCopy = 0;
                        break;

                    }

                    stackedFriend.increaseAmount(leftTillMax);
                    stackSizeCopy -= leftTillMax;
                }

                if(stackSizeCopy == 0){
                    entity.remove();
                    return null;
                }

                ArrayList<Integer> stackedAmountEntities = new ArrayList<>();
                int stackAmountCopy = stackSizeCopy;

                while (stackAmountCopy > 0) {

                    if (stackAmountCopy > finalSizeLimit) {

                        stackAmountCopy -= finalSizeLimit;
                        stackedAmountEntities.add(finalSizeLimit);
                        continue;

                    }
                    stackedAmountEntities.add(stackAmountCopy);
                    break;
                }

                List<AStacked> spawnedEntities = new ArrayList<>();
                sync(() -> {

                    boolean entityUsed = false;
                    for(int stack : stackedAmountEntities){

                        if(!entityUsed){

                            entityUsed = true;
                            spawnedEntities.add(new StackedEntity(entity, stack));

                        } else {

                            //Randomize the Location
                            Location randLoc = location.clone();
                            randLoc.setZ(randLoc.getZ() + ThreadLocalRandom.current().nextDouble(2));
                            randLoc.setX(randLoc.getX() + ThreadLocalRandom.current().nextDouble(2));

                            Entity spawnedEntity = randLoc.getWorld().spawn(randLoc, entity.getClass(), e -> EntityController.getInstance().ignore(e.getUniqueId()));
                            spawnedEntities.add(new StackedEntity(spawnedEntity, stack));
                            EntityController.getInstance().unIgnore(spawnedEntity);

                        }

                    }

                });

            }

            return null;
        }
    }

}
