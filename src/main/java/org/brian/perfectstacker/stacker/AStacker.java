package org.brian.perfectstacker.stacker;

import org.brian.core.helper.Helper;
import org.brian.perfectstacker.entity.AStacked;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.Map;

public abstract class AStacker implements Helper {

    public Class<? extends Entity> entityClass;
    public abstract AStacked stack(Entity entity, Map<String, Object> data, int stackSize);
    public AStacker(Class<? extends Entity> entityClass){
        this.entityClass = entityClass;
        Stackers.register(this);
    }

    public Class<? extends Entity> entity(){
        return entityClass;
    }

}
