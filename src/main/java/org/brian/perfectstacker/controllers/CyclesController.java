package org.brian.perfectstacker.controllers;

import org.brian.core.energiser.Energiser;
import org.brian.perfectstacker.PerfectStacker;
import org.brian.perfectstacker.cycles.StackedUpdateTask;

import java.util.concurrent.TimeUnit;

public class CyclesController {

    public CyclesController(){

        StackedUpdateTask stackedUpdateTask = new StackedUpdateTask();
        Energiser.getInstance().runRepeatingTask(stackedUpdateTask, 250, TimeUnit.MILLISECONDS);

    }

}
