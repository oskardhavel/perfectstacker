package org.brian.perfectstacker.controllers.serialized;

import org.brian.perfectstacker.controllers.StorageController;
import org.brian.perfectstacker.entity.AStacked;
import org.brian.perfectstacker.entity.custom.StackedEntity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class SerializedStorage implements Serializable {

    private static final long serialVersionUID = 252525747474L;
    private List<String> entities = new ArrayList<>();
    private StorageController storageController;

    public SerializedStorage(){}

    public SerializedStorage(StorageController controller){

        this.storageController = controller;

    }

    private void writeObject(ObjectOutputStream stream) throws IOException {

        entities.clear();
        storageController.stackedCopy().
                stream().
                filter(se -> se.bukkitEntity() != null || !se.bukkitEntity().isDead()).
                forEach(se -> {

                    entities.add(se.bukkitEntity().getUniqueId().toString() + ":" + se.stackAmount());

                });

        stream.writeObject(entities);

    }

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {

        entities = (List<String>) stream.readObject();
        StorageController.getInstance().load(entities);

    }

}
