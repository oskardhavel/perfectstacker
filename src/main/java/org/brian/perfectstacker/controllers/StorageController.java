package org.brian.perfectstacker.controllers;

import org.brian.core.database.OOPDatabase;
import org.brian.core.database.SRowType;
import org.brian.core.database.types.MYSQLDatabase;
import org.brian.core.database.types.SQLiteDatabase;
import org.brian.core.energiser.Energiser;
import org.brian.core.helper.Helper;
import org.brian.core.utils.Assert;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.perfectstacker.PerfectStacker;
import org.brian.perfectstacker.controllers.serialized.SerializedStorage;
import org.brian.perfectstacker.entity.AStacked;
import org.brian.perfectstacker.entity.custom.StackedEntity;
import org.brian.perfectstacker.entity.custom.StackedItem;
import org.brian.perfectstacker.utils.HChunk;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;

import java.io.*;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StorageController implements Helper {

    private final OOPDatabase database;
    private static StorageController instance;
    private PerfectStacker perfectStacker;
    private ConfigurationSection storageSection;
    private SerializedStorage serializedStorage;
    private List<AStacked> stacked = new ArrayList<>();

    private final String TABLE_NAME;

    public StorageController(PerfectStacker perfectStacker){

        instance = this;
        this.perfectStacker = perfectStacker;
        this.serializedStorage = new SerializedStorage(this);

        storageSection = perfectStacker.config().oopConfig().section("storage").getFirst();
        TABLE_NAME = storageSection.value("table name").toString() + "Entities";
        if(storageSection.value("type").toString().equalsIgnoreCase("SQLITE")){

            //Using SQLITE for storing entities

            database = new SQLiteDatabase(perfectStacker.getDataFolder(), storageSection.value("database").toString());
            initDatabase();

        } else {

            //Using MYSQL for storing entities
            database = new MYSQLDatabase(storageSection.valueAsRequired("host"), storageSection.valueAsRequired("port"), storageSection.valueAsRequired("database"), storageSection.valueAsRequired("user"), storageSection.valueAsRequired("password"));
            initDatabase();

        }

        Energiser.getInstance().runRepeatingTask(this::save, 20, TimeUnit.SECONDS);

    }

    private void initDatabase() {

        //Create all the tables
        Assert.assertTrue("Database failed to initialize!", database != null);

        database.createTable(TABLE_NAME, "id INTEGER PRIMARY KEY AUTOINCREMENT", "data " + SRowType.TEXT);
        load();

    }

    public static StorageController getInstance() {
        return instance;
    }

    public List<AStacked> stackedCopy() {
        return new ArrayList<>(stacked);
    }

    public List<AStacked> stacked() {
        return stacked;
    }

    public List<AStacked> byWorld(World world){

        return stackedCopy().
                stream().
                filter(stacked -> stacked.location().getWorld() == world).
                collect(Collectors.toList());

    }

    public List<AStacked> byFilter(Predicate<AStacked> filter){

        return stackedCopy().
                stream().
                filter(filter).
                collect(Collectors.toList());

    }

    public void register(AStacked aStacked) {
        stacked.add(aStacked);
    }

    public void remove(AStacked aStacked) {
        stacked.remove(aStacked);
    }

    public boolean isStacked(Entity entity) {
        return this.stackedCopy().stream().anyMatch(stackedEntity -> stackedEntity.bukkitEntity() == entity);
    }

    public AStacked get(Entity entity) {
        return this.stackedCopy().stream().filter(stackedEntity -> stackedEntity.bukkitEntity() == entity).findFirst().orElse(null);
    }


    public void load(List<String> entities) {

        for(String serializedEntity : entities){

            String split[] = serializedEntity.split(":");

            Entity entity = Bukkit.getEntity(UUID.fromString(split[0]));
            if(entity == null) continue;

            int stackSize = Integer.parseInt(split[1]);

            if(entity instanceof Item) new StackedItem(((Item) entity), stackSize);
            else new StackedEntity(entity, stackSize);

        }

    }

    private boolean loaded = false;

    public void save(){

        if(!loaded) return;

        database.cleanTable(TABLE_NAME);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(serializedStorage);
            oos.close();

            String serializedStorage = Base64.getEncoder().encodeToString(baos.toByteArray());
            PreparedStatement ps = database.getConnection().prepareStatement("INSERT INTO " + TABLE_NAME + " (data) VALUES (?)");
            ps.setString(1, serializedStorage);
            ps.executeUpdate();

        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public void load(){

        Energiser.getInstance().runTaskDelayed(() -> {
            loaded = true;
            Map<Integer, HashMap<String, Object>> data = database.getAllRows(TABLE_NAME);

            data.values().forEach(map -> {

                String serializedEntity = map.get("data").toString();
                byte [] bytes = Base64.getDecoder().decode(serializedEntity);
                try {

                    ObjectInputStream ois = new ObjectInputStream(
                            new ByteArrayInputStream(bytes));
                    SerializedStorage serializedStorage = ((SerializedStorage) ois.readObject());

                } catch (Exception ex){
                    ex.printStackTrace();
                }
            });
        }, 3, TimeUnit.SECONDS);

    }

}
