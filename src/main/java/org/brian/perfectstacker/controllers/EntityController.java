package org.brian.perfectstacker.controllers;

import org.bukkit.entity.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EntityController {

    private static EntityController instance;
    private static List<UUID> ignoredEntities = new ArrayList<>();

    public EntityController(){

        if(instance == null) instance = this;

    }

    public void ignore(Entity entity) {
        ignore(entity.getUniqueId());
    }

    public void ignore(UUID uuid) {
        ignoredEntities.add(uuid);
    }

    public boolean isIgnored(Entity entity){
        return isIgnored(entity.getUniqueId());
    }

    public boolean isIgnored(UUID uuid){
        return ignoredEntities.contains(uuid);
    }

    public void unIgnore(Entity entity){
        unIgnore(entity.getUniqueId());
    }

    public void unIgnore(UUID uuid){
        ignoredEntities.remove(uuid);
    }

    public static EntityController getInstance() {
        return instance;
    }
}
