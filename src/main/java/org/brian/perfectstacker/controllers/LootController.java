package org.brian.perfectstacker.controllers;

import com.google.gson.Gson;
import org.brian.core.helper.Helper;
import org.brian.core.material.OOPMaterial;
import org.brian.core.utils.JarUtil;
import org.brian.perfectstacker.PerfectStacker;
import org.brian.perfectstacker.drop.ADrop;
import org.brian.perfectstacker.lootTable.LootTable;
import org.brian.perfectstacker.lootTable.LootTableItem;
import org.brian.perfectstacker.lootTable.function.LootTableFunction;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LootController implements Helper {

    private static LootController instance;
    private Map<EntityType, LootTable> lootTableMap = new HashMap<>();
    private Map<EntityType, List<ADrop>> customDropMap = new HashMap<>();

    public LootController(PerfectStacker perfectStacker) {

        instance = this;
        File lootTableFolder = new File(perfectStacker.getDataFolder() + "/lootTable");

        try {

            JarUtil.copyFolderFromJar("lootTable", perfectStacker.getDataFolder(), JarUtil.CopyOption.REPLACE_IF_EXIST);
            Gson gson = new Gson();

            for (File entityFile : lootTableFolder.listFiles()) {

                LootTable table = parseFile(entityFile, gson);
                if(table == null) continue;

                lootTableMap.put(table.owner(), table);

            }

            print("&eLoaded (" + this.lootTableMap.size() + ") Loot Tables!");

        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<ItemStack> defaultDrops(EntityType type) {

        List<ItemStack> dropList = new ArrayList<>();
        if (lootTableMap.containsKey(type)) {
            dropList.addAll(lootTableMap.get(type).generateItems());
        }

        return dropList;
    }

    public List<ADrop> customDrops(EntityType type){

        List<ADrop> dropList = new ArrayList<>();
        if(customDropMap.containsKey(type)){
            dropList.addAll(customDropMap.get(type));
        }

        return dropList;

    }

    public void register(ADrop drop){

        for(EntityType entityType : drop.entityTypes()){

            if(!customDropMap.containsKey(entityType)){

                List<ADrop> dropList = new ArrayList<>();
                dropList.add(drop);

                customDropMap.put(entityType, dropList);

            } else customDropMap.get(entityType).add(drop);

        }

    }

    public static LootController getInstance() {
        return instance;
    }

    private LootTable parseFile(File file, Gson GSON){

        LootTable table = null;

        try {

            Map<String, Object> entityData = new HashMap<>();
            entityData = GSON.fromJson(new FileReader(file), entityData.getClass());

            EntityType entityType = EntityType.fromName(entityData.get("owner").toString());
            if(entityType == null) return null;
            table = new LootTable(entityType);

            for(Object dropObj : toList(entityData.get("drops"))){

                Map<String, Object> dropData = toMap(dropObj);
                Material material = OOPMaterial.fromString(dropData.get("material").toString()).parseMaterial();
                if(material == null) continue;

                LootTableItem lootTableItem = new LootTableItem(material);

                for(Object functionObj : toList(dropData.get("functionList"))){

                    Map<String, Object> functionData = toMap(functionObj);
                    LootTableFunction.FunctionType functionType = LootTableFunction.FunctionType.valueOf(functionData.get("type").toString());

                    int min = (int) Math.round((double) functionData.get("min"));
                    int max = (int) Math.round((double) functionData.get("max"));

                    lootTableItem.addFunction(new LootTableFunction(functionType, min, max));

                }

                table.addLootTableItem(lootTableItem);

            }

        } catch (Exception ex){
            ex.printStackTrace();
        }

        return table;

    }

    private Map<String, Object> toMap(Object object){
        return (Map<String, Object>)object;
    }

    private List<Object> toList(Object object){
        return (List<Object>) object;
    }

}
