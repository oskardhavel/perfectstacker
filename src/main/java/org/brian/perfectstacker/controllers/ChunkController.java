package org.brian.perfectstacker.controllers;

import org.brian.perfectstacker.PerfectStacker;
import org.brian.perfectstacker.utils.HChunk;
import org.bukkit.Chunk;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.Map;

public class ChunkController {

    private static ChunkController instance;
    private PerfectStacker perfectStacker;
    private Map<Chunk, HChunk> cache = new HashMap<>();

    public ChunkController(PerfectStacker perfectStacker){

        instance = this;
        this.perfectStacker = perfectStacker;

    }

    public static ChunkController getInstance() {
        return instance;
    }

    public HChunk chunk(Chunk chunk) {
        //TODO("CHECK IF CHUNK IS LOADED");
        if(cache.containsKey(chunk)) return cache.get(chunk);
        else return generate(chunk);

    }

    public HChunk chunk(Location location) {
        //TODO("CHECK IF CHUNK IS LOADED");
        return chunk(location.getChunk());
    }

    private HChunk generate(Chunk chunk) {
        HChunk hChunk = new HChunk(chunk);
        cache.put(chunk, hChunk);
        return hChunk;
    }

}
