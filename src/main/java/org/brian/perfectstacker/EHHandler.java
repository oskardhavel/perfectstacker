package org.brian.perfectstacker;


import org.brian.core.helper.Helper;

import java.util.logging.ErrorManager;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class EHHandler extends Handler implements Helper {

    public EHHandler(){
        setErrorManager(new EHErrorManager());
    }

    @Override
    public void publish(LogRecord logRecord) {
    }

    @Override
    public void flush() {

    }

    @Override
    public void close() throws SecurityException {

    }
    public class EHErrorManager extends ErrorManager {

        @Override
        public synchronized void error(String msg, Exception ex, int code) {
            print("");
            print("&cException was thrown in " + PerfectStacker.getInstance().getName() + ": " + ex.getMessage());

            for(StackTraceElement ste : ex.getStackTrace()){
                print("&c - " + ste.toString());
            }

            print("");
        }
    }
}
