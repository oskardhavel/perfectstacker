package org.brian.perfectstacker;

import org.brian.core.Core;
import org.brian.core.energiser.Energiser;
import org.brian.core.helper.Helper;
import org.brian.core.yaml.OOPConfiguration;
import org.brian.perfectstacker.configuration.PSConfig;
import org.brian.perfectstacker.controllers.*;
import org.brian.perfectstacker.events.MechanicsListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class PerfectStacker extends JavaPlugin implements Helper {

    private static PerfectStacker instance;
    private PSConfig config;

    @Override
    public void onEnable() {

        instance = this;
        new Core(this);
        new Energiser("Energiser", new EHHandler());
        new LootController(this);
        initConfig();
        new StorageController(this);
        new CyclesController();
        new ChunkController(this);
        new EntityController();
        new MechanicsListener(this);

    }

    @Override
    public void onDisable() {

        instance = null;
        Energiser.getInstance().shutdown();

    }

    public static PerfectStacker getInstance() {
        return instance;
    }
    public PSConfig config() {
        return config;
    }

    private void initConfig(){

        File configFile = new File(getDataFolder(), "config.yml");
        if(!configFile.exists()) saveResource("config.yml", true);

        //Update Config
//        File currentConfigFile = new File(getDataFolder(), "currentConfig.yml");
//        OOPConfiguration currentConfig = new OOPConfiguration(currentConfigFile);
//        saveResource("config.yml", true);
//        OOPConfiguration updatedConfig = new OOPConfiguration(new File(getDataFolder(), "config.yml"));
//
//        ConfigurationUpdater updater = new ConfigurationUpdater(updatedConfig, currentConfig);
//        int updatedValues = updater.update();
//
//        print("UPDATED VALUES: " + updatedValues);
//
//        //Reset
//        FileUtils.deleteQuietly(new File(getDataFolder(), "config.yml"));
//        currentConfigFile.renameTo(new File(getDataFolder(), "config.yml"));
//
//        currentConfig.save();
        this.config = new PSConfig(new OOPConfiguration(new File(getDataFolder(), "config.yml")));

    }

}
