package org.brian.perfectstacker.utils;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

public class Direction {

    public static String getCardinalDirection(Location location) {
        double rotation = (location.getYaw() - 90) % 360;
        if (rotation < 0) {
            rotation += 360.0;
        }
        if (0 <= rotation && rotation < 22.5) {
            return "NORTH";
        } else if (22.5 <= rotation && rotation < 67.5) {
            return "NORTH_EAST";
        } else if (67.5 <= rotation && rotation < 112.5) {
            return "EAST";
        } else if (112.5 <= rotation && rotation < 157.5) {
            return "SOUTH_EAST";
        } else if (157.5 <= rotation && rotation < 202.5) {
            return "SOUTH";
        } else if (202.5 <= rotation && rotation < 247.5) {
            return "SOUTH_WEST";
        } else if (247.5 <= rotation && rotation < 292.5) {
            return "WEST";
        } else if (292.5 <= rotation && rotation < 337.5) {
            return "NORTH_WEST";
        } else if (337.5 <= rotation && rotation < 360.0) {
            return "NORTH";
        } else {
            return null;
        }
    }

    public static Location faceLocation(Entity entity, Location to) {
        if (entity.getWorld() != to.getWorld()) {
            return null;
        }
        Location fromLocation = entity.getLocation();

        double xDiff = to.getX() - fromLocation.getX();
        double yDiff = to.getY() - fromLocation.getY();
        double zDiff = to.getZ() - fromLocation.getZ();

        double distanceXZ = Math.sqrt(xDiff * xDiff + zDiff * zDiff);
        double distanceY = Math.sqrt(distanceXZ * distanceXZ + yDiff * yDiff);

        double yaw = Math.toDegrees(Math.acos(xDiff / distanceXZ));
        double pitch = Math.toDegrees(Math.acos(yDiff / distanceY)) - 90.0D;
        if (zDiff < 0.0D) {
            yaw += Math.abs(180.0D - yaw) * 2.0D;
        }
        Location loc = entity.getLocation();
        loc.setYaw((float) (yaw - 90.0F));
        loc.setPitch((float) (pitch - 90.0F));
        return loc;
    }

}
