package org.brian.perfectstacker.utils;

import org.apache.commons.lang3.StringUtils;

public class StringUtil {

    public static String beautify(String string){

        string = StringUtils.replace(string, "_", " ");
        string = StringUtils.capitalize(string.toLowerCase());

        return string;

    }

}
