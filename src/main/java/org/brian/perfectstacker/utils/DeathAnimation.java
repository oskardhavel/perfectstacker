package org.brian.perfectstacker.utils;

import net.minecraft.server.v1_13_R2.*;
import org.brian.core.energiser.Energiser;
import org.brian.core.nbt.NBTEntity;
import org.brian.core.utils.ReflectionUtils;
import org.brian.perfectstacker.PerfectStacker;
import org.bukkit.EntityEffect;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DeathAnimation {

    //Entity
    private static Class<?> LIVING_ENTITY;
    private static Method LIVING_ENTITY_GET_HANDLE;
    private static Method LIVING_ENTITY_ID_METHOD;

    //Death animation
    private static Constructor<?> ENTITY_STATUS_PACKET_CONSTRUCTOR;
    private static Constructor<?> ENTITY_DESTROY_PACKET;

    //SPAWN ANIMATION
    private static Constructor<?> ENTITY_SPAWN_PACKET_CONSTRUCTOR;

    //Player Stuff
    private static Method PLAYER_GET_HANDLE_METHOD;
    private static Method PLAYER_SEND_PACKET_METHOD;
    private static Field PLAYER_CONNECTION_FIELD;

    private static boolean init = false;
    static {

        if(!init) {
            init = true;

            try {

                //Living Entity Stuff
                LIVING_ENTITY = ReflectionUtils.Package.CB_ENTITY.getClass("CraftLivingEntity");
                LIVING_ENTITY_GET_HANDLE = ReflectionUtils.method(LIVING_ENTITY, "getHandle");

                //Required Classes
                Class<?> NMS_ENTITY_CLASS = ReflectionUtils.Package.MINECRAFT_SERVER.getClass("Entity");
                Class<?> NMS_LIVING_ENTITY = ReflectionUtils.Package.MINECRAFT_SERVER.getClass("EntityLiving");
                Class<?> STATUS_PACKET_CLASS = ReflectionUtils.Package.MINECRAFT_SERVER.getClass("PacketPlayOutEntityStatus");
                Class<?> SPAWN_PACKET_CLASS = ReflectionUtils.Package.MINECRAFT_SERVER.getClass("PacketPlayOutSpawnEntityLiving");
                Class<?> PLAYER_CONNECTION_CLASS = ReflectionUtils.Package.MINECRAFT_SERVER.getClass("PlayerConnection");
                Class<?> PACKET_CLASS = ReflectionUtils.Package.MINECRAFT_SERVER.getClass("Packet");
                Class<?> ENTITY_PLAYER_CLASS = ReflectionUtils.Package.MINECRAFT_SERVER.getClass("EntityPlayer");
                Class<?> CRAFT_PLAYER_CLASS = ReflectionUtils.Package.CB_ENTITY.getClass("CraftPlayer");
                Class<?> CRAFT_LIVING_ENTITY = ReflectionUtils.Package.CB_ENTITY.getClass("CraftLivingEntity");
                Class<?> DESTROY_PACKET_CLASS = ReflectionUtils.Package.MINECRAFT_SERVER.getClass("PacketPlayOutEntityDestroy");

                //Packets
                ENTITY_STATUS_PACKET_CONSTRUCTOR = ReflectionUtils.getConstructor(STATUS_PACKET_CLASS, NMS_ENTITY_CLASS, byte.class);
                ENTITY_SPAWN_PACKET_CONSTRUCTOR = ReflectionUtils.getConstructor(SPAWN_PACKET_CLASS, NMS_LIVING_ENTITY);

                int array[] = new int[1];
                ENTITY_DESTROY_PACKET = ReflectionUtils.getConstructor(DESTROY_PACKET_CLASS, array.getClass());

                //Player
                PLAYER_CONNECTION_FIELD = ReflectionUtils.field(ENTITY_PLAYER_CLASS, true, "playerConnection");
                PLAYER_GET_HANDLE_METHOD = ReflectionUtils.method(CRAFT_PLAYER_CLASS, "getHandle");
                PLAYER_SEND_PACKET_METHOD = ReflectionUtils.method(PLAYER_CONNECTION_CLASS, "sendPacket", PACKET_CLASS);

                //Entity
                LIVING_ENTITY_GET_HANDLE = ReflectionUtils.method(CRAFT_LIVING_ENTITY, "getHandle");
                LIVING_ENTITY_ID_METHOD = ReflectionUtils.method(CRAFT_LIVING_ENTITY, "getEntityId");

            } catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }

    static void sendPacket(Player player, Object packet) throws Exception {

        Object ENTITY_PLAYER = PLAYER_GET_HANDLE_METHOD.invoke(player);
        Object PLAYER_CONNECTION = PLAYER_CONNECTION_FIELD.get(ENTITY_PLAYER);
        PLAYER_SEND_PACKET_METHOD.invoke(PLAYER_CONNECTION, packet);

    }

    public static void sendAnimation(LivingEntity entity, List<Player> receivers, boolean respawn) throws Exception {

        //Send Death Animation
        Object ENTITY_LIVING = LIVING_ENTITY_GET_HANDLE.invoke(entity);
        int ids[] = new int[1];
        ids[0] = (int) LIVING_ENTITY_ID_METHOD.invoke(entity);

        NBTEntity nbtEntity = new NBTEntity(entity);
        boolean hadNoAI = nbtEntity.getByte("NoAI") == 1;
        if(!hadNoAI) nbtEntity.setByte("NoAI", (byte)1);

        byte DEATH_ANIM_CODE = 3;
        Object DEATH_PACKET = ENTITY_STATUS_PACKET_CONSTRUCTOR.newInstance(ENTITY_LIVING, DEATH_ANIM_CODE);
        for(Player player : receivers) {
            sendPacket(player, DEATH_PACKET);
        }

        Energiser.getInstance().runTaskDelayed(() -> {

            try{
                Object DESTROY_PACKET = ENTITY_DESTROY_PACKET.newInstance(ids);
                for(Player player : receivers) {
                    sendPacket(player, DESTROY_PACKET);
                }

            } catch (Exception ex){
                ex.printStackTrace();
            }

        }, 220, TimeUnit.MILLISECONDS);

//        if(respawn) {
//            Energiser.getInstance().runTaskDelayed(() -> {
//
//                try {
//
//                    Object SPAWN_PACKET = ENTITY_SPAWN_PACKET_CONSTRUCTOR.newInstance(ENTITY_LIVING);
//                    for (Player player : receivers) {
//                        sendPacket(player, SPAWN_PACKET);
//                    }
//
//                    if(!hadNoAI) nbtEntity.setByte("NoAI", (byte)0);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//
//            }, 1, TimeUnit.SECONDS);
//        } else {
//            Energiser.getInstance().runTaskDelayed(() -> Energiser.getInstance().runSyncTask(() -> entity.remove(), PerfectStacker.getInstance()), 100, TimeUnit.MILLISECONDS);
//        }

    }

}
