package org.brian.perfectstacker.utils;

import org.brian.core.utils.AsyncUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class HChunk implements Serializable {

    private static final long serialVersionUID = 264254567892132627L;

    private int x;
    private int z;
    private String world;

    public HChunk(Chunk chunk){

        this.x = chunk.getX();
        this.z = chunk.getZ();
        this.world = chunk.getWorld().getName();

    }

    public Chunk build(){

        //Safety Checks
        if(Bukkit.getWorld(world) == null) return null;
        return Bukkit.getWorld(world).getChunkAt(x,z);

    }

    public boolean isLoaded(){

        //Safety Checks
        if(Bukkit.getWorld(world) == null) return false;

        return AsyncUtils.isChunkLoaded(getX(), getZ(), getWorld());
    }

    public boolean is(Location location) {

        double chunkX = location.getBlockX() >> 4;
        double chunkZ = location.getBlockZ() >> 4;

        return chunkX == x && chunkZ == z && location.getWorld().getName().equalsIgnoreCase(world);

    }

    public boolean is(HChunk chunk) {

        return chunk.getX() == x && chunk.getZ() == z && chunk.getWorld() != null && chunk.getWorld().getName().equalsIgnoreCase(world);

    }

    public boolean is(Chunk chunk) {

        return chunk.getX() == x && chunk.getZ() == z && chunk.getWorld().getName().equalsIgnoreCase(world);

    }

    public int getZ() {
        return z;
    }

    public int getX() {
        return x;
    }

    public World getWorld() {

        //Safety Checks
        if(Bukkit.getWorld(world) == null) return null;
        return Bukkit.getWorld(world);

    }

    private void writeObject(ObjectOutputStream stream) throws IOException {


        stream.writeInt(x);
        stream.writeInt(z);
        stream.writeObject(world);

    }

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {


        this.x = stream.readInt();
        this.z = stream.readInt();
        this.world = stream.readObject().toString();

    }


}
