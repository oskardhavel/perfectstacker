package org.brian.perfectstacker.lootTable;

import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class LootTable {

    private EntityType owner;
    private List<LootTableItem> drops = new ArrayList<>();

    public LootTable(EntityType owner){
        this.owner = owner;
    }

    public LootTable addLootTableItem(LootTableItem tableItem){
        this.drops.add(tableItem);
        return this;
    }

    public EntityType owner(){
        return owner;
    }

    public List<LootTableItem> lootTableItemList(){
        return drops;
    }

    public List<ItemStack> generateItems(){

        List<ItemStack> generatedItems = new ArrayList<>();

        for(LootTableItem lootTableItem : lootTableItemList()){
            generatedItems.add(lootTableItem.generateItem());
        }

        return generatedItems;

    }

}
