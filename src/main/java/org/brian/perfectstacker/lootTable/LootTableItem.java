package org.brian.perfectstacker.lootTable;

import org.brian.perfectstacker.lootTable.function.LootTableFunction;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class LootTableItem {

    private Material material;
    private List<LootTableFunction> functionList = new ArrayList<>();

    public LootTableItem(Material material){

        this.material = material;

    }

    public Material material(){
        return material;
    }

    public List<LootTableFunction> functionList(){
        return functionList;
    }

    public LootTableItem addFunction(LootTableFunction function){
        this.functionList.add(function);
        return this;
    }

    public ItemStack generateItem(){

        LootTableFunction function = functionList().
                stream().
                filter(f -> f.type() == LootTableFunction.FunctionType.SET_COUNT).
                findFirst().orElse(null);

        return new ItemStack(material, function != null ? function.generate() : 1);

    }

}
