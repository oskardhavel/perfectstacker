package org.brian.perfectstacker.events;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.brian.core.energiser.Energiser;
import org.brian.core.events.async.AsyncEvent;
import org.brian.core.helper.Helper;
import org.brian.core.nbt.NBTItem;
import org.brian.core.utils.InventoryUtil;
import org.brian.perfectstacker.PerfectStacker;
import org.brian.perfectstacker.controllers.EntityController;
import org.brian.perfectstacker.controllers.LootController;
import org.brian.perfectstacker.controllers.StorageController;
import org.brian.perfectstacker.drop.ADrop;
import org.brian.perfectstacker.entity.custom.StackedEntity;
import org.brian.perfectstacker.entity.custom.StackedItem;
import org.brian.perfectstacker.events.custom.StackedEntityDeathEvent;
import org.brian.perfectstacker.stacker.Stackers;
import org.brian.perfectstacker.utils.DeathAnimation;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.entity.*;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MechanicsListener
        implements Helper {

    public MechanicsListener(PerfectStacker stackerPlugin) {

        new AsyncEvent<>(ItemMergeEvent.class).
                preAsync((event, data) -> {

                    //Merging item Check
                    NBTItem mergeNBT = new NBTItem(event.getEntity().getItemStack());
                    if(mergeNBT.hasKey("PS_ITEM")) event.setCancelled(true);

                    //Target item Check
                    NBTItem targetNBT = new NBTItem(event.getTarget().getItemStack());
                    if(targetNBT.hasKey("PS_ITEM")) event.setCancelled(true);

                }).
                priority(EventPriority.LOWEST).
                register();

        new AsyncEvent<>(EntitySpawnEvent.class).
                preAsync((event, data) -> {

                    if (event.isCancelled()) return;
                    if(!(event.getEntity() instanceof Item)) return;

                    NBTItem nbtitem = new NBTItem(((Item) event.getEntity()).getItemStack());
                    if(nbtitem.hasKey("PS_ITEM")) return;

                    nbtitem.setInteger("PS_ITEM", ThreadLocalRandom.current().nextInt(9999));
                    ((Item) event.getEntity()).setItemStack(nbtitem.getItem());

                    data.putData("entity", event.getEntity());
                    data.putData("chunk", event.getEntity().getLocation().getChunk());
                    data.putData("location", event.getEntity().getLocation());

                }).
                async((event, data) -> {
                   if(data.containsData("entity")) {
                       Stackers.stack(data.getData("entity"), data.getData());

                       //Removing mark
                       Item item = data.getData("entity");
                       NBTItem nbtItem = new NBTItem(item.getItemStack());
                       nbtItem.removeKey("PS_ITEM");
                       item.setItemStack(nbtItem.getItem());

                   }
                }).
                priority(EventPriority.LOWEST).
                register();


        new AsyncEvent<>(EntityDamageEvent.class).
                preAsync((event, data) -> {

                    if (event.getEntity() instanceof LivingEntity && StorageController.getInstance().isStacked(event.getEntity())) {

                        data.putData("isStacked", true);

                        double damage = event.getFinalDamage();
                        if (((LivingEntity)event.getEntity()).getHealth() - damage <= 0.0) {

                            if(PerfectStacker.getInstance().config().entitiesSection().isDeathAnimation()) return;
                            data.putData("isDamageFinal", true);
                            event.setCancelled(true);

                        }
                    }
                }).async((event, data) -> {

                    if(data.containsData("isStacked")) {

                        if(data.containsData("isDamageFinal")) {

                            StackedEntity stackedEntity = (StackedEntity) StorageController.getInstance().get(event.getEntity());
                            List<ItemStack> defaultDropList = LootController.getInstance().defaultDrops(event.getEntityType());
                            List<ADrop> customDropList = LootController.getInstance().customDrops(event.getEntityType());

                            World entityWorld = event.getEntity().getLocation().getWorld();
                            Location entityLocation = event.getEntity().getLocation();
                            List<Player> bukkitPlayers = new ArrayList<>(Bukkit.getOnlinePlayers());

                            EntityDeathEvent deathEvent = new EntityDeathEvent((LivingEntity) event.getEntity(), defaultDropList);
                            Bukkit.getPluginManager().callEvent(deathEvent);

                            stackedEntity.decreaseAmount(1);
                            boolean isFinal = false;

                            if (stackedEntity.stackAmount() == 0) {

                                isFinal = true;
                                StorageController.getInstance().remove(stackedEntity);
                                sync(stackedEntity::remove);

                            }

                            StackedEntityDeathEvent sede = new StackedEntityDeathEvent(stackedEntity, defaultDropList, customDropList, isFinal, ((LivingEntity) event.getEntity()).getKiller());

                            if(isFinal) ((LivingEntity) event.getEntity()).setHealth(((LivingEntity) event.getEntity()).getMaxHealth());

                            sync(() -> {

                                //Custom Drop List
                                sede.customDropList().forEach(drop -> drop.execute(sede));

                                //Default Drop List
                                sede.defaultDropList().forEach(item -> entityWorld.dropItem(entityLocation, item));

                            });

                        }

                    }


                }).
                priority(EventPriority.LOWEST).
                register();

        new AsyncEvent<>(EntityDeathEvent.class).
                preAsync((event, data) -> {

                    if (event.getEntity() instanceof LivingEntity && StorageController.getInstance().isStacked(event.getEntity())) {

                        if (!PerfectStacker.getInstance().config().entitiesSection().isDeathAnimation()) return;
                        data.putData("isStacked", true);
                        event.getDrops().clear();

                    }

                }).
                priority(EventPriority.LOWEST).
                async((event, data) -> {

                    if(data.containsData("isStacked")){

                        StackedEntity stackedEntity = (StackedEntity) StorageController.getInstance().get(event.getEntity());
                        List<ItemStack> defaultDropList = LootController.getInstance().defaultDrops(event.getEntityType());
                        List<ADrop> customDropList = LootController.getInstance().customDrops(event.getEntityType());

                        World entityWorld = event.getEntity().getLocation().getWorld();
                        Location entityLocation = event.getEntity().getLocation();
                        List<Player> bukkitPlayers = new ArrayList<>(Bukkit.getOnlinePlayers());

                        stackedEntity.decreaseAmount(1);
                        boolean isFinal = false;

                        if (stackedEntity.stackAmount() == 0) {

                            isFinal = true;
                            stackedEntity.bukkitEntity().setCustomNameVisible(false);
                            StorageController.getInstance().remove(stackedEntity);
                            sync(stackedEntity::remove);

                        }

                        StackedEntityDeathEvent sede = new StackedEntityDeathEvent(stackedEntity, defaultDropList, customDropList, isFinal, event.getEntity().getKiller());

                        if(!isFinal) {

                            stackedEntity.bukkitEntity().setCustomNameVisible(false);
                            stackedEntity.lock();
                            sync(() -> {

                                LivingEntity spawnedEntity = entityLocation.getWorld().spawn(entityLocation, event.getEntity().getClass(), entity -> {
                                    EntityController.getInstance().ignore(entity.getUniqueId());
                                });

                                stackedEntity.bukkitEntity(spawnedEntity);
                                EntityController.getInstance().unIgnore(spawnedEntity.getUniqueId());
                                stackedEntity.unLock();

                            });

                        }

                        sync(() -> {
                            //Custom Drop List
                            sede.customDropList().forEach(drop -> drop.execute(sede));

                            //Default Drop List
                            sede.defaultDropList().forEach(item -> entityWorld.dropItem(entityLocation, item));
                        });

                    }

                }).register();

        new AsyncEvent<>(CreatureSpawnEvent.class).
                preAsync((event, data) -> {

                    if (event.isCancelled()) return;
                    if(EntityController.getInstance().isIgnored(event.getEntity())) return;

                    data.putData("entity", event.getEntity());
                    data.putData("chunk", event.getEntity().getLocation().getChunk());
                    data.putData("location", event.getEntity().getLocation());

                }).
                async((event, data) -> {
                    if(data.containsData("entity")) Stackers.stack(data.getData("entity"), data.getData(), 1);
                }).
                register();

        new AsyncEvent<>(PlayerPickupItemEvent.class).
                preAsync((event, data) -> {

                    if(StorageController.getInstance().isStacked(event.getItem())) {

                        event.setCancelled(true);
                        StackedItem stackedItem = (StackedItem) StorageController.getInstance().get(event.getItem());
                        Player player = event.getPlayer();

                        data.putData("inventory", player.getInventory());
                        data.putData("stackedItem", stackedItem);

                    }

                }).
                async((event, data) -> {

                    if(!data.containsData("inventory")) return;

                    StackedItem stackedItem = data.getData("stackedItem");

                    int added = InventoryUtil.addItem(((Inventory) data.getData("inventory")), stackedItem.itemList());
                    int stackAmount = stackedItem.stackAmount();
                    if (stackAmount == added) stackedItem.remove();
                    else stackedItem.decreaseAmount(added);

                }).
                register();
        new AsyncEvent<>(EntityBreedEvent.class).
                preAsync((event, data) -> {

                    print("BREED EVENT: " + event.getEntity());

                    if(!StorageController.getInstance().isStacked(event.getEntity())) return;
                    if(EntityController.getInstance().isIgnored(event.getEntity())) return;


                }).
                register();
    }
}
