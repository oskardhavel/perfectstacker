package org.brian.perfectstacker.events.custom;

import org.brian.perfectstacker.drop.ADrop;
import org.brian.perfectstacker.entity.custom.StackedEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class StackedEntityDeathEvent extends Event {
    private static HandlerList handlerList = new HandlerList();
    private StackedEntity stackedEntity;
    private List<ItemStack> defaultDropList;
    private List<ADrop> customDropList;
    private boolean isFinal;
    private Player killer;

    public StackedEntityDeathEvent(StackedEntity stackedEntity, List<ItemStack> dropList, List<ADrop> customDrops, boolean isFinal, Player killer) {
        this.stackedEntity = stackedEntity;
        this.defaultDropList = dropList;
        this.isFinal = isFinal;
        this.customDropList = customDrops;
        this.killer = killer;
    }

    public HandlerList getHandlers() {
        return handlerList;
    }

    public List<ItemStack> defaultDropList() {
        return this.defaultDropList;
    }

    public List<ADrop> customDropList(){
        return customDropList;
    }

    public StackedEntity stackedEntity() {
        return this.stackedEntity;
    }

    public Player killer(){
        return killer;
    }

    public Entity bukkitEntity(){
        return stackedEntity.bukkitEntity();
    }

    public boolean isFinal() {
        return isFinal;
    }
}
