package org.brian.perfectstacker.drop;

import org.brian.core.helper.Helper;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.perfectstacker.controllers.LootController;
import org.brian.perfectstacker.drop.types.CommandDrop;
import org.brian.perfectstacker.drop.types.ItemDrop;
import org.brian.perfectstacker.events.custom.StackedEntityDeathEvent;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public abstract class ADrop implements Helper {

    private int chance = 100;
    private List<EntityType> entityTypes;

    public int chance(){
        return chance;
    }

    public ADrop chance(int chance){
        this.chance = chance;
        return this;
    }

    public List<EntityType> entityTypes(){
        return entityTypes;
    }

    public ADrop entityType(List<EntityType> types){
        this.entityTypes = types;
        return this;
    }

    public static ADrop fromSection(ConfigurationSection section){

        if(section.isPresentValue("material")) return new ItemDrop(section);
        else return new CommandDrop(section);

    }

    public abstract void execute(StackedEntityDeathEvent event);

    public void register(){
        LootController.getInstance().register(this);
    }

    public boolean roll(){

        if(chance == 100) return true;
        return ThreadLocalRandom.current().nextInt(100) < chance;

    }

}
