package org.brian.perfectstacker.drop.types;

import org.apache.commons.lang3.StringUtils;
import org.brian.core.utils.Assert;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.perfectstacker.drop.ADrop;
import org.brian.perfectstacker.events.custom.StackedEntityDeathEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.ArrayList;
import java.util.List;

public class CommandDrop extends ADrop {

    private List<String> commandList = new ArrayList<>();

    public CommandDrop(ConfigurationSection section){

        Assert.assertTrue("(Drop: " + section.key() +") Command value not found!", section.isPresentValue("command"));

        List<EntityType> typeList = new ArrayList<>();
        if(section.value("entity") instanceof String){
            Assert.assertTrue("(Drop: " + section.key() + ") Entity by name " + section.value("entity").toString() + " is not present!", arrayContains(EntityType.valueOf(section.value("entity").toString().toUpperCase()), EntityType.values()));
            typeList.add(EntityType.valueOf(((String) section.value("entity")).toUpperCase()));
        } else {
            List<String> stringList = section.valueAsRequired("entity");
            stringList.forEach(e -> Assert.assertTrue("(Drop: " + section.key() + ") Entity by name " + section.value("entity").toString() + " is not present!", arrayContains(EntityType.valueOf(e.toUpperCase()), EntityType.values())));
            stringList.forEach(e -> typeList.add(EntityType.valueOf(e.toUpperCase())));
        }

        if(section.value("command") instanceof String) commandList.add(section.value("command").toString());
        else commandList.addAll(section.valueAsRequired("command"));

        if(section.isPresentValue("chance")) chance(section.valueAsRequired("chance"));
        entityType(typeList);
        register();
    }

    @Override
    public void execute(StackedEntityDeathEvent event) {

        if(event.killer() == null) return;
        if(!roll()) return;

        String entityType = StringUtils.capitalize(event.bukkitEntity().getType().name().toLowerCase());
        String playerName = event.killer().getName();

        commandList.forEach(command -> {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.
                    replace("%player%", playerName).
                    replace("%entityType%", entityType));
        });

    }
}
