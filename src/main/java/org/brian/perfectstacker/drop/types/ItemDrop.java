package org.brian.perfectstacker.drop.types;

import org.brian.core.item.OOPItemBuilder;
import org.brian.core.utils.Assert;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.perfectstacker.drop.ADrop;
import org.brian.perfectstacker.events.custom.StackedEntityDeathEvent;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ItemDrop extends ADrop {

    private ItemStack item;

    public ItemDrop(ConfigurationSection section){

        Assert.assertTrue("Entity of drop " + section.key() + " is not found!", section.isPresentValue("entity"));

        List<EntityType> typeList = new ArrayList<>();
        if(section.value("entity") instanceof String){
            Assert.assertTrue("(Drop: " + section.key() + ") Entity by name " + section.value("entity").toString() + " is not present!", arrayContains(EntityType.valueOf(section.value("entity").toString().toUpperCase()), EntityType.values()));
            typeList.add(EntityType.valueOf(((String) section.value("entity")).toUpperCase()));
        } else {
            List<String> stringList = section.valueAsRequired("entity");
            stringList.forEach(e -> Assert.assertTrue("(Drop: " + section.key() + ") Entity by name " + section.value("entity").toString() + " is not present!", arrayContains(EntityType.valueOf(e.toUpperCase()), EntityType.values())));
            stringList.forEach(e -> typeList.add(EntityType.valueOf(e.toUpperCase())));
        }

        this.item = OOPItemBuilder.fromMap(section.values(true)).buildItem();
        if(section.isPresentValue("chance")) chance(section.valueAsRequired("chance"));

        entityType(typeList);
        register();

    }

    public ItemStack item(){
        return item;
    }

    @Override
    public void execute(StackedEntityDeathEvent event) {
        boolean rolled = roll();
        if(!rolled) return;
        event.defaultDropList().add(item.clone());
    }
}
