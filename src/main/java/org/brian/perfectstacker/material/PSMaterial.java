package org.brian.perfectstacker.material;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class PSMaterial {

    private int data = 0;
    private Material material;

    public PSMaterial(Material material, int data) {
        this.data = data;
        this.material = material;
    }

    public Material material() {
        return material;
    }

    public int data() {
        return data;
    }

    public ItemStack item() {
        return new ItemStack(material, 1, data == -1 ? (byte)0 : (byte)data);
    }

}
