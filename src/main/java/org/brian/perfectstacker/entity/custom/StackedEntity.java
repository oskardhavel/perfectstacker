package org.brian.perfectstacker.entity.custom;

import org.brian.core.nbt.NBTEntity;
import org.brian.perfectstacker.PerfectStacker;
import org.brian.perfectstacker.entity.AStacked;
import org.brian.perfectstacker.utils.Direction;
import org.brian.perfectstacker.utils.StringUtil;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.Map;

public class StackedEntity extends AStacked {

    private NBTEntity nbtEntity = null;

    public StackedEntity(Entity entity, int amount){
        super(entity, amount);
    }

    @Override
    public void updateAmount() {

        sync(() -> {

            bukkitEntity().setCustomNameVisible(true);
            bukkitEntity().setCustomName(ChatColor.translateAlternateColorCodes('&', "&ex" + stackAmount() + " &7" + StringUtil.beautify(bukkitEntity().getType().name())));

        });

    }

    @Override
    protected void selfUpdate() {

        if(nbtEntity == null) nbtEntity = new NBTEntity(bukkitEntity());
        updateNoAI();
        if(nbtEntity.getByte("NoAI") == 1) {

            Location location = bukkitEntity().getLocation();
            if(location.getYaw() != 90 || location.getPitch() != 0) {

                location.setYaw(90);
                location.setPitch(0);
                bukkitEntity().teleport(location);

            }

        }

    }

    public void updateNBT(){
        this.nbtEntity = new NBTEntity(bukkitEntity());
    }

    private void updateNoAI(){

        Map<EntityType, Boolean> noAI = PerfectStacker.getInstance().config().entitiesSection().noAI();
        boolean setNoAI;

        if(noAI.containsKey(type())) setNoAI = noAI.get(type());
        else setNoAI = PerfectStacker.getInstance().config().entitiesSection().defaultNoAI();

        byte bit = setNoAI ? (byte)1 : (byte)0;

        if(nbtEntity.getByte("NoAI") != bit) nbtEntity.setByte("NoAI", bit);

    }

    public StackedEntity(Entity entity){
        super(entity, 1);
    }

}
