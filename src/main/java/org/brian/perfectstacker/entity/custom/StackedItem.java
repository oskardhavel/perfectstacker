package org.brian.perfectstacker.entity.custom;

import org.brian.perfectstacker.entity.AStacked;
import org.brian.perfectstacker.utils.StringUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StackedItem extends AStacked {

    private List<ItemStack> cache = new ArrayList<>();
    private long lastStackSize;

    public StackedItem(Item item, int amount){
        super(item, amount);
    }

    public StackedItem(Item item){
        super(item, 1);
    }

    @Override
    protected void selfUpdate() {

    }

    @Override
    public void updateAmount() {

        if(stackAmount() <= 0) {
            remove();
            return;
        }

        sync(() -> {

            bukkitEntity().setCustomNameVisible(true);
            bukkitEntity().setCustomName(ChatColor.translateAlternateColorCodes('&', "&ex" + stackAmount() + " &7" + StringUtil.beautify(((Item)bukkitEntity()).getItemStack().getType().name())));

        });
        itemList();

    }

    public List<ItemStack> itemList() {

        if(cache != null && lastStackSize == stackAmount() && !cache.isEmpty()) return cache.
                stream().
                map(ItemStack::clone).
                collect(Collectors.toList());
        if(cache != null) cache.clear();

        int copySize = stackAmount();
        List<ItemStack> itemStackList = new ArrayList<>();
        Material material = bukkitItem().getItemStack().getType();
        byte data = (byte) bukkitItem().getItemStack().getDurability();

        while (copySize > 0) {

            if(copySize > 64){

                itemStackList.add(new ItemStack(material, 64, data));
                copySize -= 64;

            } else {

                itemStackList.add(new ItemStack(material, copySize, data));
                copySize = 0;

            }

        }

        cache = itemStackList.
                stream().
                map(ItemStack::clone).
                collect(Collectors.toList());
        return itemStackList;

    }

    public Item bukkitItem(){
        return (Item)bukkitEntity();
    }

}
