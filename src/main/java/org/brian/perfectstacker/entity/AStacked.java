package org.brian.perfectstacker.entity;

import org.brian.core.helper.Helper;
import org.brian.core.utils.AsyncUtils;
import org.brian.perfectstacker.controllers.ChunkController;
import org.brian.perfectstacker.controllers.StorageController;
import org.brian.perfectstacker.entity.custom.StackedEntity;
import org.brian.perfectstacker.utils.HChunk;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class AStacked implements Helper {

    private int stackedAmount;
    private Entity entity;
    private UUID entityUUID;
    private World world;
    private EntityType entityType;
    private boolean locked = false;

    private List<HChunk> watchedChunks;
    private HChunk entityChunk;
    private int previousChunkX, previousChunkZ = -1;
    private boolean chunkLoaded = false;

    //HARD CODED
    private int watchRange = 1;

    public AStacked(Entity entity, int stackedAmount){
        this.entity = entity;
        this.entityType = entity.getType();
        this.entityUUID = entity.getUniqueId();
        this.stackedAmount = stackedAmount;
        this.world = entity.getLocation().getWorld();
        update();
        updateAmount();
        StorageController.getInstance().register(this);
    }

    public void update() {

        if(locked) return;

        if(entity == null || entity.isDead()) {

            if(entity != null && Bukkit.getEntity(entityUUID) != null) this.entity = Bukkit.getEntity(entityUUID);
            else {

                //Remove from the Storage
                remove();
                return;

            }

        }

        //TODO("CHECK IF CHUNK IS LOADED, IF IT'S NOT DON'T UPDATE!")

        Location entityLocation = entity.getLocation();
        int chunkX = entityLocation.getBlockX() >> 4;
        int chunkZ = entityLocation.getBlockZ() >> 4;

        if(chunkX != previousChunkX || chunkZ != previousChunkZ || watchedChunks == null){

            //Update WatchedChunks
            watchedChunks = AsyncUtils.getChunksAround(chunkX, chunkZ, watchRange, entityLocation.getWorld(), false).
                    stream().
                    map(chunk -> ChunkController.getInstance().chunk(chunk)).
                    collect(Collectors.toList());
            this.entityChunk = ChunkController.getInstance().chunk(entityLocation.getChunk());

        }

        //Update Values For Next Update
        previousChunkX = chunkX;
        previousChunkZ = chunkZ;
        chunkLoaded = true;

        selfUpdate();

    }

    public Location location() {
        return entity.getLocation();
    }

    public int stackAmount() {
        return stackedAmount;
    }

    public void stackAmount(int stackedAmount){
        this.stackedAmount = stackedAmount;
        updateAmount();
    }

    public void increaseAmount(int increase) {
        this.stackedAmount += increase;
        this.updateAmount();
    }

    public void decreaseAmount(int amount) {
        this.stackedAmount -= amount;
        this.updateAmount();
    }

    public EntityType type(){
        return entityType;
    }

    public Entity bukkitEntity(){
        return entity;
    }

    public void bukkitEntity(Entity entity){
        this.entity = entity;
        this.entityUUID = entity.getUniqueId();
        if(this instanceof StackedEntity) ((StackedEntity) this).updateNBT();
        updateAmount();
    }

    public List<HChunk> watchedChunks(){
        if(watchedChunks == null) return new ArrayList<>();
        return new ArrayList<>(watchedChunks);
    }

    public void remove() {

        StorageController.getInstance().remove(this);
        if(isAsyncThread()) sync(() -> bukkitEntity().remove());
        else bukkitEntity().remove();

    }

    public void lock(){
        this.locked = true;
    }

    public void unLock(){
        this.locked = false;
    }

    public abstract void updateAmount();
    protected abstract void selfUpdate();

}
