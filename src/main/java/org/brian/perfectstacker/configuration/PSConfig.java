package org.brian.perfectstacker.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.brian.core.material.OOPMaterial;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.OOPConfiguration;
import org.brian.perfectstacker.drop.ADrop;
import org.brian.perfectstacker.material.PSMaterial;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntityDamageEvent;

public class PSConfig {

    private ItemsSection itemsSection;
    private EntitiesSection entitiesSection;
    private OOPConfiguration configuration;

    public PSConfig(OOPConfiguration configuration) {
        this.configuration = configuration;
        this.itemsSection = new ItemsSection(configuration.section("Items stacking").getFirst());
        this.entitiesSection = new EntitiesSection(configuration.section("Entities stacking").getFirst());
    }

    public ItemsSection itemsSection() {
        return itemsSection;
    }

    public EntitiesSection entitiesSection(){
        return entitiesSection;
    }

    public class ItemsSection {

        private boolean enabled;
        private Map<PSMaterial, Double> mergeRadius = new HashMap<>();
        private double defaultMergeRadius = 10;
        private String customName;
        private Map<PSMaterial, Integer> stackLimit = new HashMap<>();
        private int defaultStackLimit = 100;
        private List<PSMaterial> blacklist = new ArrayList<>();

        public ItemsSection(ConfigurationSection itemsSection) {

            double radius;
            this.enabled = itemsSection.valueAsRequired("enabled");
            List<String> serialized_merge_radius = itemsSection.valueAsRequired("merge radius");

            for (String value : serialized_merge_radius) {

                Material material;
                String[] split = value.split(":");

                if (split[0].equalsIgnoreCase("default")) {
                    this.defaultMergeRadius = Double.parseDouble(split[1]);
                    continue;
                }

                if (split.length == 3) {
                    material = OOPMaterial.fromString(split[0]).parseMaterial();
                    int data = Integer.parseInt(split[1]);
                    radius = Double.parseDouble(split[2]);
                    this.mergeRadius.put(new PSMaterial(material, data), radius);
                    continue;
                }
                if (split.length != 2) continue;

                material = OOPMaterial.fromString(split[0]).parseMaterial();
                double radius2 = Double.parseDouble(split[1]);
                this.mergeRadius.put(new PSMaterial(material, -1), radius2);

            }

            this.customName = itemsSection.valueAsRequired("custom name");
            List<String> serialized_limit = itemsSection.valueAsRequired("size limit");
            for (String value : serialized_limit) {

                String[] split = value.split(":");

                if (split[0].equalsIgnoreCase("default")) {
                    this.defaultStackLimit = Integer.parseInt(split[1]);
                    continue;
                }

                if (split.length == 3) {

                    Material material = OOPMaterial.fromString(split[0]).parseMaterial();
                    int data = Integer.parseInt(split[1]);
                    int radius3 = Integer.parseInt(split[2]);
                    this.stackLimit.put(new PSMaterial(material, data), radius3);
                    continue;

                }
                if (split.length != 2) continue;

                Material material = OOPMaterial.fromString(split[0]).parseMaterial();
                int radius4 = Integer.parseInt(split[1]);
                this.stackLimit.put(new PSMaterial(material, -1), radius4);

            }

            List<String> serialized_blacklist = itemsSection.valueAsRequired("blacklist");
            for (String value : serialized_blacklist) {

                String[] split = value.split(":");
                if (split.length == 2) {

                    Material material = OOPMaterial.fromString(split[0]).parseMaterial();
                    int data = Integer.parseInt(split[1]);
                    this.blacklist.add(new PSMaterial(material, data));
                    continue;

                }
                if (split.length != 1) continue;

                Material material = OOPMaterial.fromString(split[0]).parseMaterial();
                this.blacklist.add(new PSMaterial(material, -1));

            }
        }

        public boolean isEnabled() {
            return this.enabled;
        }

        public double defaultMergeRadius() {
            return this.defaultMergeRadius;
        }

        public Map<PSMaterial, Double> mergeRadius() {
            return this.mergeRadius;
        }

        public String customName() {
            return this.customName;
        }

        public Map<PSMaterial, Integer> limit() {
            return this.stackLimit;
        }

        public int defaultLimit() {
            return this.defaultStackLimit;
        }

        public List<PSMaterial> blacklist() {
            return this.blacklist;
        }
    }

    public class EntitiesSection {

        private boolean enabled;
        private Map<EntityType, Double> mergeRadius = new HashMap<>();
        private double defaultMergeRadius = 10;
        private String customName;
        private Map<EntityType, Integer> stackLimit = new HashMap<>();
        private int defaultStackLimit = 100;
        private List<EntityType> blacklist = new ArrayList<>();

        private boolean deathAnimation = false;
        private boolean oneStack = true;
        private List<EntityDamageEvent.DamageCause> stackDeathOn = new ArrayList<>();

        private Map<EntityType, Boolean> noAI = new HashMap<>();
        private boolean defaultNoAI = false;

        public EntitiesSection(ConfigurationSection section){

            this.enabled = section.valueAsRequired("enabled");
            List<String> serialized_merge_radius = section.valueAsRequired("merge radius");
            for (String value : serialized_merge_radius) {

                String[] split = value.split(":");
                if (split[0].equalsIgnoreCase("default")) {
                    this.defaultMergeRadius = Double.parseDouble(split[1]);
                    continue;
                } else if (split.length == 2) {
                    this.mergeRadius.put(EntityType.valueOf(split[0].toUpperCase()), Double.parseDouble(split[1]));
                }

            }

            this.customName = section.valueAsRequired("custom name");

            List<String> serialized_limit = section.valueAsRequired("size limit");
            for (String value : serialized_limit) {

                String[] split = value.split(":");
                if (split[0].equalsIgnoreCase("default")) {
                    this.defaultStackLimit = Integer.parseInt(split[1]);
                    continue;
                } else if (split.length == 2) {
                    this.stackLimit.put(EntityType.valueOf(split[0].toUpperCase()), Integer.valueOf(split[1]));
                }
            }

            List<String> serialized_blacklist = section.valueAsRequired("blacklist");
            for (String value : serialized_blacklist) {
                this.blacklist.add(EntityType.valueOf(value.toUpperCase()));
            }

            if(section.isPresentValue("one stack")) oneStack = section.valueAsRequired("one stack");
            if(section.isPresentValue("death animation")) deathAnimation = section.valueAsRequired("death animation");
            if(section.isPresentValue("stack death on")) {

                //Initialize da list


            }

            if(section.isPresentChild("drops")){

                for(ConfigurationSection dropSection : section.child("drops").childs().values()){
                    ADrop.fromSection(dropSection);
                }

            }

            if(section.isPresentValue("noAI")){

                for(String serializedAI : ((List<String>) section.value("noAI"))){

                    String split[] = serializedAI.split(":");

                    if(split[0].equalsIgnoreCase("default")) defaultNoAI = Boolean.parseBoolean(split[1]);
                    else {
                        noAI.put(EntityType.valueOf(split[0].toUpperCase()), Boolean.parseBoolean(split[1]));
                    }

                }

            }

        }

        public boolean isEnabled() {
            return enabled;
        }

        public double defaultMergeRadius() {
            return defaultMergeRadius;
        }

        public boolean isDeathAnimation() {
            return deathAnimation;
        }

        public boolean isOneStack() {
            return oneStack;
        }

        public int defaultStackLimit() {
            return defaultStackLimit;
        }

        public List<EntityType> blacklist(){
            return blacklist;
        }

        public Map<EntityType, Boolean> noAI(){
            return noAI;
        }

        public boolean defaultNoAI() {
            return defaultNoAI;
        }

        public Map<EntityType, Double> mergeRadius(){
            return mergeRadius;
        }

        public Map<EntityType, Integer> stackLimit(){
            return stackLimit;
        }

    }


    public OOPConfiguration oopConfig(){
        return configuration;
    }

}
