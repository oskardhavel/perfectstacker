package org.brian.perfectstacker.cycles;

import org.brian.perfectstacker.controllers.StorageController;
import org.brian.perfectstacker.entity.AStacked;

public class StackedUpdateTask implements Runnable {

    @Override
    public void run() {

        StorageController.getInstance().stackedCopy().forEach(AStacked::update);

    }
}
